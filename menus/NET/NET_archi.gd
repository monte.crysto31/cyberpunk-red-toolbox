extends Widget


@onready var subnet_list    = $SubNetSelection/TabContainer
@onready var subnet_display = $HBoxContainer/SubNetDisplay

@onready var ui_theme     = preload("res://assets/resources/main_theme.tres")
@onready var subnet_entry = preload("res://menus/NET/NET_entry.tscn")


func _ready():
	
	# widget behaviour
	SignalBus.change_widget.connect(toggle)
	
	# subnet selection
	
	$SubNetSelection/HBoxContainer/AddSubNetCat.pressed.connect($AddSubNetCat.show_add_category.bind(
		"SubNETs",
		null))
	$SubNetSelection/HBoxContainer/AddSubNet   .pressed.connect($AddSubNet.show_add_entry.bind(
		"SubNETs",
		null,
		["file", "image"]))
	
	$AddSubNetCat.category_added.connect(update)
	$AddSubNet   .entry_added   .connect(update)
	
	# subnet controls
	
	$HBoxContainer/SubNetControls/GridContainer/NameEdit.text_changed.connect(
		func (new_text: String) : subnet_display.NET.name = new_text)
	$HBoxContainer/SubNetControls/GridContainer/DiffEdit.item_selected.connect(
		func (index: int) : subnet_display.NET.difficulty = index)
	
	$HBoxContainer/SubNetControls/GridContainer/HBoxContainer/VBoxContainer/MoveUp.pressed.connect(
		func () :
			subnet_display.curr_cell    = subnet_display.NET.move_up()
			subnet_display.visible_cell = subnet_display.curr_cell
			subnet_display.update())
	$HBoxContainer/SubNetControls/GridContainer/HBoxContainer/VBoxContainer/MoveDown.pressed.connect(
		func () :
			subnet_display.curr_cell = subnet_display.NET.move_down()
			subnet_display.visible_cell = subnet_display.curr_cell
			subnet_display.update())
	$HBoxContainer/SubNetControls/GridContainer/HBoxContainer/MoveRight.pressed.connect(
		func () :
			subnet_display.curr_cell = subnet_display.NET.move_right()
			subnet_display.visible_cell = subnet_display.curr_cell
			subnet_display.update())
	
	$HBoxContainer/SubNetControls/GenerateNET.pressed.connect(
		func () :
			subnet_display.NET.generate()
			subnet_display.update())
	$HBoxContainer/SubNetControls/RollFloors.pressed.connect(
		func () :
			subnet_display.NET.roll_all_floors()
			subnet_display.update())
	$HBoxContainer/SubNetControls/ShowNET.pressed.connect(
		func () :
			SignalBus.show_remote_NET.emit(subnet_display))
	$HBoxContainer/SubNetControls/HBoxContainer/SaveNET.pressed.connect(
		func () :
			DataLoader.add_entry(
				"SubNETs",
				null,
				subnet_display.NET.category,
				subnet_display.NET.name,
				subnet_display.NET.serialize_NET(),
				true))
	$HBoxContainer/SubNetControls/HBoxContainer/CloseNET.pressed.connect(
		func () :
			subnet_display.NET = SubNET.new()
			$HBoxContainer  .hide()
			$SubNetSelection.show()
			update())
	
	$HBoxContainer/SubNetControls/DeleteNET.pressed.connect($ConfirmDelete.show)
	$ConfirmDelete.confirmed.connect(
		func () :
			DataLoader.delete_entry(
				"SubNETs",
				null,
				subnet_display.NET.category,
				subnet_display.NET.name)
			$HBoxContainer  .hide()
			$SubNetSelection.show()
			update())
	
	# other
	
	subnet_display.get_node("ProgramDesc/MarginContainer/VBoxContainer/CloseDesc").pressed.connect(
		func () :
			subnet_display.get_node("ProgramDesc").curr_program = {}
			subnet_display.update())


func delete_category(cat_name: String):
	
	DataLoader.delete_category("SubNETs", null, cat_name)
	
	update()

func add_subnet(
	cat_name:    String,
	subnet_name: String,
):
	
	DataLoader.add_entry(
		"SubNETs",
		null,
		cat_name,
		subnet_name,
		{})
	
	update()

func delete_subnet(
	cat_name:    String,
	subnet_name: String
):
	
	DataLoader.delete_entry(
		"SubNETs",
		null,
		cat_name,
		subnet_name)
	
	update()


func show_subnet(
	cat_name:    String,
	subnet_name: String
):
	
	subnet_display.NET.deserialize_NET(
		cat_name,
		subnet_name,
		DataLoader.data["SubNETs"][cat_name][subnet_name])
	
	$HBoxContainer/SubNetControls/GridContainer/DiffEdit.select(subnet_display.NET.difficulty)
	$HBoxContainer/SubNetControls/GridContainer/NameEdit.text = subnet_display.NET.name
	subnet_display.update()
	
	$SubNetSelection.hide()
	$HBoxContainer  .show()


func update():
	
	DataLoader.load_section("SubNETs")
	
	for category in subnet_list.get_children():
			
		subnet_list.remove_child(category)
		category.queue_free()
	
	for cat_name in DataLoader.data["SubNETs"].keys():
		
		var category_node   = FlowContainer.new()
		category_node.theme = ui_theme
		category_node.name  = cat_name
		category_node.add_theme_constant_override("h_separation", 0)
		category_node.add_theme_constant_override("v_separation", 0)
		
		for subnet_name in DataLoader.data["SubNETs"][cat_name].keys():
	
			var subnet_entry_inst = subnet_entry.instantiate()
			subnet_entry_inst.cat_name    = cat_name
			subnet_entry_inst.subnet_name = subnet_name
			subnet_entry_inst.connect("subnet_selected", show_subnet)
			subnet_entry_inst.connect("delete_subnet", delete_subnet)
			
			category_node.add_child(subnet_entry_inst)
		
		if DataLoader.data["SubNETs"][cat_name].keys().size() == 0:
			
			var delete_category_button = $DeleteCategory.duplicate()
			delete_category_button.cat_name = cat_name
			delete_category_button.connect("delete_category", delete_category)
			delete_category_button.show()
			
			category_node.add_child(delete_category_button)
		
		subnet_list.add_child(category_node)
