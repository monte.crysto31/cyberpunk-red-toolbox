extends Control


var cat_name :    String = ""
var subnet_name : String = ""

signal subnet_selected(
	cat_name:    String,
	subnet_name: String)

signal delete_subnet(
	cat_name:    String,
	subnet_name: String)


func _ready():
	
	$VBoxContainer/Label.text = subnet_name
	
	$VBoxContainer/Button.connect(
		"mouse_entered",
		func () : $VBoxContainer/Label.add_theme_color_override("font_color", Color("80e4ff")))
	$VBoxContainer/Button.connect(
		"mouse_exited",
		func () : $VBoxContainer/Label.add_theme_color_override("font_color", Color("ff413d")))
	$VBoxContainer/Button.connect(
		"pressed",
		send_subnet_selected)


func send_subnet_selected():
	
	subnet_selected.emit(
		self.cat_name,
		self.subnet_name)


func send_delete_subnet():
	
	delete_subnet.emit(
		self.cat_name,
		self.subnet_name)
