extends ColorRect


var cat_name    = ""
var subnet_name = ""

signal add_subnet(
	cat_name    : String,
	subnet_name : String)


func _ready():
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.connect("item_selected", update_cat_name)
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.connect("text_changed", update_subnet_name)
	
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Cancel.connect("pressed", self.hide)
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Add.connect("pressed", send_add_subnet)


func update_cat_name(new_cat: int):
	
	self.cat_name = $PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.get_item_text(new_cat)
	print(self.cat_name)


func update_subnet_name(new_name: String):
	
	subnet_name = new_name


func send_add_subnet():
	
	add_subnet.emit(
		cat_name,
		subnet_name)
	
	self.hide()


func show_add_subnet():
	
	subnet_name = ""
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.clear()
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.clear()
	
	for i in DataLoader.data["SubNETs"].size():
		
		$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.add_item(DataLoader.data["SubNETs"].keys()[i], i)
		
		# init at first item
		if i == 0:
			cat_name = DataLoader.data["SubNETs"].keys()[i]
	
	self.show()
