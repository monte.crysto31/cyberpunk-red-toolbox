extends ColorRect


var cat_name : String = ""

signal add_category(new_cat_name: String)


func _ready():
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.connect("text_changed", update_cat_name)
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Cancel.connect("pressed", self.hide)
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Add.connect("pressed", send_add_category)


func update_cat_name(new_name: String):
	
	cat_name = new_name


func send_add_category():
	
	add_category.emit(cat_name)


func show_add_category():
	
	cat_name = ""
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.text = ""
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.grab_focus()
	
	self.show()
