extends MarginContainer


var cat_name : String

signal delete_category(empty_cat_name: String)


func _ready():
	
	$Delete.connect("pressed", send_delete_category)


func send_delete_category():
	
	delete_category.emit(cat_name)
