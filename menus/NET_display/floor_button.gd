class_name FloorButton
extends TextureButton


var floor_id : String = ""

var is_curr_floor = false
var anim_dur = 0.5
var anim_acc = 0.0


func _process(delta):
	
	if is_curr_floor:
		
		anim_acc = fmod(anim_acc + delta , anim_dur)
		var displacement = (1. - absf(cos(2.*PI * anim_acc))) * $TextureRect1.size.x
		
		$TextureRect1.position = Vector2( displacement,  displacement)
		$TextureRect2.position = Vector2(-displacement,  displacement) + Vector2(self.size.x - $TextureRect1.size.x, 0)
		$TextureRect3.position = Vector2( displacement, -displacement) + Vector2(0, self.size.y - $TextureRect1.size.y)
		$TextureRect4.position = Vector2(-displacement, -displacement) + self.size - $TextureRect1.size


func select_floor():
	
	$TextureRect1.show()
	$TextureRect2.show()
	$TextureRect3.show()
	$TextureRect4.show()
	
	$TextureRect1.position = self.position + Vector2(0, 0)
	$TextureRect2.position = self.position + Vector2(self.size.x, 0)
	$TextureRect3.position = self.position + Vector2(0, self.size.y)
	$TextureRect4.position = self.position + self.size
	
	self.is_curr_floor = true

func unselect_floor():
	
	$TextureRect1.hide()
	$TextureRect2.hide()
	$TextureRect3.hide()
	$TextureRect4.hide()
	
	self.is_curr_floor = false
