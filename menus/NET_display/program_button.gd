extends TextureButton


var info : Dictionary = {}


func set_info(program_name:String):
	
	if program_name.contains(" DD "):
		
		info = RollTables.NET_nodes[program_name.split(" DD ")[0]].duplicate(true)
		info["DD"] = program_name.split(" DD ")[1]
		
		$DD.show()
		$DD/Label.text = info["DD"]
	
	else:
		
		info = RollTables.Programs[program_name].duplicate(true)
		
		$DD.hide()
	
	info["name"] = program_name
	
	$TextureRect.texture = info.get("img", null)


func get_info() -> Dictionary:
	
	return info
