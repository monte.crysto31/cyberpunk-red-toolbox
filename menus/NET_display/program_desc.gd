extends PanelContainer


var curr_program : Dictionary = {}
var subwindow : bool = false


func update():
	
	var prog_name = ""
	
	if curr_program.has("class"):
		prog_name = "%s (%s)" % [curr_program.get("name", "Program"), curr_program["class"]]
	else:
		prog_name = curr_program.get("name", "Program")
	
	$MarginContainer/VBoxContainer/Name \
		.text = prog_name
	
	$MarginContainer/VBoxContainer/HBoxContainer/Image \
		.texture = curr_program.get("img", null)
	
	if not curr_program.has("PER") or curr_program["PER"] == -1:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/PER.hide()
	else:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/PER.show()
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/PER \
			.text = "PER: %d" % curr_program["PER"]
	
	if not curr_program.has("SPD") or curr_program["SPD"] == -1:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/SPD.hide()
	else:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/SPD.show()
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/SPD \
			.text = "SPD: %d" % curr_program["SPD"]
	
	if not curr_program.has("ATK") or curr_program["ATK"] == -1:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/ATK.hide()
	else:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/ATK.show()
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/ATK \
			.text = "ATK: %d" % curr_program["ATK"]
	
	if not curr_program.has("DEF") or curr_program["DEF"] == -1:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/DEF.show()
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/DEF.hide()
	else:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/DEF \
			.text = "DEF: %d" % curr_program["DEF"]
	
	if not curr_program.has("REZ") or curr_program["REZ"] == -1:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/REZ.hide()
	else:
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/REZ.show()
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/REZ \
			.text = "REZ: %d" % curr_program["REZ"]
	
	$MarginContainer/VBoxContainer/Desc.text = curr_program.get("desc", "No description available")
	
	if subwindow:
		$MarginContainer/VBoxContainer/CloseDesc.hide()
