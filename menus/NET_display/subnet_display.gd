extends HBoxContainer


var NET          : SubNET = SubNET.new()

var curr_cell    : String = "1_1"
var visible_cell : String = ""

var subwindow : bool   = false


func ensure_curr_cell_visible():
	
	var cell: Control
	
	for node in $ScrollContainer/GridContainer.get_children():
		if node is FloorButton and node.floor_id == self.curr_cell:
			cell = node
			break
	
	await get_tree().process_frame
	$ScrollContainer.ensure_control_visible(cell)


func show_floor_contents(floor_id:String):
	
	visible_cell = floor_id
	update()


func show_program_desc(info:Dictionary):
	
	$ProgramDesc.curr_program = info
	update()


func update():
	
	# tree architecture
	
	for node in $ScrollContainer/GridContainer.get_children():
		if not ["FloorLabel", "HLine", "VLine", "AngleLine"].has(node.name):
			$ScrollContainer/GridContainer.remove_child(node)
			node.queue_free()
	
	if NET.net_matrix.size() == 0:
		
		print("Empty NET!")
		return
	
	# 2 for each branch, +1 for the label and -1 one for the last branch
	# that will never need to split to the right
	$ScrollContainer/GridContainer.columns = 2 * NET.net_matrix[0].size()
	
	for i in NET.net_matrix.size():
		
		var FloorLabel = $ScrollContainer/GridContainer/FloorLabel.duplicate()
		FloorLabel.text = "%s    " % str(i+1)
		FloorLabel.show()
		$ScrollContainer/GridContainer.add_child(FloorLabel)
		
		# choose the cell proper
		for j in NET.net_matrix[i].size():
			
			match NET.net_matrix[i][j]:
				
				0:
					$ScrollContainer/GridContainer.add_child(Control.new())
				
				-1:
					var AngleLine = $ScrollContainer/GridContainer/AngleLine.duplicate()
					AngleLine.show()
					$ScrollContainer/GridContainer.add_child(AngleLine)
				
				-2:
					var HLine = $ScrollContainer/GridContainer/HLine.duplicate()
					HLine.show()
					$ScrollContainer/GridContainer.add_child(HLine)
				
				_:
					var floor_button = preload("res://menus/NET_display/floor_button.tscn").instantiate()
					floor_button.floor_id = "%s_%s" % [i+1, NET.net_matrix[i][j]]
					$ScrollContainer/GridContainer.add_child(floor_button)
					if not self.subwindow:
						floor_button.pressed.connect(
							self.show_floor_contents.bind(floor_button.floor_id))
					if floor_button.floor_id == self.curr_cell:
						floor_button.select_floor()
			
			# fill after the cell
			if j < NET.net_matrix[i].size()-1:
				
				if NET.net_matrix[i][j+1] < 0:
					var HLine = $ScrollContainer/GridContainer/HLine.duplicate()
					HLine.show()
					$ScrollContainer/GridContainer.add_child(HLine)
				
				else:
					$ScrollContainer/GridContainer.add_child(Control.new())
		
		# fill the line below the floor to link poperly to the next one
		if i < (NET.net_matrix.size()-1):
			
			# Label column
			$ScrollContainer/GridContainer.add_child(Control.new())
			
			# choose the proper link
			for j in NET.net_matrix[i].size():
				
				if NET.net_matrix[i+1][j] > 0:
					var VLine = $ScrollContainer/GridContainer/VLine.duplicate()
					VLine.show()
					$ScrollContainer/GridContainer.add_child(VLine)
				else:
					$ScrollContainer/GridContainer.add_child(Control.new())
				
				# fill after the link
				if j < NET.net_matrix[i].size()-1:
					$ScrollContainer/GridContainer.add_child(Control.new())
	
	# floor side view
	
	if visible_cell == "" or not NET.floors.has(visible_cell):
		$FloorList.hide()
	
	else:
		$FloorList.show()
		
		for node in $FloorList/MarginContainer/VBoxContainer.get_children():
			$FloorList/MarginContainer/VBoxContainer.remove_child(node)
			node.queue_free()
		
		for program_name in NET.floors[visible_cell]:
		
			var program_button = preload("res://menus/NET_display/program_button.tscn").instantiate()
			program_button.set_info(program_name)
			
			if not subwindow:
				program_button.pressed.connect(show_program_desc.bind(program_button.get_info().duplicate()))
			
			$FloorList/MarginContainer/VBoxContainer.add_child(program_button)
	
	# program description
	
	$ProgramDesc.subwindow = subwindow
	
	if $ProgramDesc.curr_program == {}:
		
		$ProgramDesc.hide()
	
	else:
		
		$ProgramDesc.update()
		$ProgramDesc.show()
	
	if not subwindow and Globals.is_player_view_visible and Globals.player_view_state == Globals.NET:
		SignalBus.show_remote_NET.emit(self)
