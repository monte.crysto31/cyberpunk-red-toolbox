extends ColorRect


var data_type    = ""
var data_subtype = null
var cat_name     = ""

signal category_added()


func _ready():
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.connect("text_changed", update_cat_name)
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Cancel.connect("pressed", self.hide)
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Add.connect("pressed", send_category_added)


func update_cat_name(new_name: String):
	
	self.cat_name = new_name


func send_category_added():
	
	DataLoader.add_category(
		self.data_type,
		self.data_subtype,
		self.cat_name)
	
	self.hide()
	
	category_added.emit()


func show_add_category(type:String, subtype:Variant):
	
	self.data_type    = type
	self.data_subtype = subtype
	self.cat_name     = ""
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.text = ""
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.grab_focus()
	
	if self.data_subtype == null:
		$PanelContainer/MarginContainer/VBoxContainer/Label.text = "Add %s Category" % self.data_type
	else:
		$PanelContainer/MarginContainer/VBoxContainer/Label.text = "Add %s Category" % self.data_subtype
	
	self.show()
