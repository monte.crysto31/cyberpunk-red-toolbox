extends ColorRect


var data_type    : String  = ""
var data_subtype : Variant = null
var data_cat     : String  = ""
var entry_name   : String  = ""
var file_path    : String  = ""
var image_path   : String  = ""

var hidden_fields : Array = []
var overwrite     : bool = false

signal entry_added()


func _ready():
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit \
		.connect("item_selected", update_data_cat)
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit \
		.connect("text_changed", update_entry_name)
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/FileEdit \
		.connect("pressed", $FileDialog.show)
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/ImageEdit \
		.connect("image_selected", func (path) : self.image_path = path)
	
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Cancel.connect("pressed", self.hide)
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Add.connect("pressed", send_entry_added)
	
	$FileDialog.connect("file_selected", select_file_path)


func update_data_cat(new_cat:int):
	
	self.data_cat = $PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.get_item_text(new_cat)


func update_entry_name(new_name:String):
	
	self.entry_name = new_name


func send_entry_added():
	
	var entry_data = {
		"type"     : self.data_type,
		"category" : self.data_cat,
		"name"     : self.entry_name,
	}
	
	if self.data_subtype != null:
		entry_data["subtype"] = self.data_subtype
	
	if not "file" in hidden_fields:
		entry_data["file"] = self.file_path
	if not "image" in hidden_fields:
		entry_data["image"] = self.image_path
	
	DataLoader.add_entry(
		self.data_type,
		self.data_subtype,
		self.data_cat,
		self.entry_name,
		entry_data,
		self.overwrite)
	
	entry_added.emit()
	self.hide()


func select_file_path(path:String):
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/FileEdit.text = path
	self.file_path = path


func hide_fields():
	
	for field in self.hidden_fields:
		
		match field:
			
			"file":
				$PanelContainer/MarginContainer/VBoxContainer/GridContainer/FileLabel.hide()
				$PanelContainer/MarginContainer/VBoxContainer/GridContainer/FileEdit.hide()
			"image":
				$PanelContainer/MarginContainer/VBoxContainer/GridContainer/ImageLabel.hide()
				$PanelContainer/MarginContainer/VBoxContainer/GridContainer/ImageEdit.hide()


func show_add_entry(
	type:String,
	subtype:Variant,
	fields:Array = []
):
	
	self.data_type     = type
	self.data_subtype  = subtype
	self.entry_name    = ""
	self.file_path     = ""
	self.image_path    = ""
	self.hidden_fields = fields
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.clear()
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.clear()
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/FileEdit.text = ""
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.grab_focus()
	
	var cat_list = []
	
	if data_subtype == null:
		cat_list = DataLoader.data[data_type].keys()
		$PanelContainer/MarginContainer/VBoxContainer/Label.text = "Add %s" % self.data_type
	else:
		cat_list = DataLoader.data[data_type][data_subtype].keys()
		$PanelContainer/MarginContainer/VBoxContainer/Label.text = "Add %s %s" % [self.data_subtype, self.data_type]
	
	for i in cat_list.size():
		
		$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.add_item(cat_list[i], i)
		
		# init at first item
		if i == 0:
			self.data_cat = cat_list[i]
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/FileEdit.text = ""
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/ImageEdit.select_img_file("")
	
	hide_fields()
	self.show()


func show_edit_entry(
	old_data_type:String,
	old_data_subtype:Variant,
	old_data_cat:String,
	old_entry_name:String,
	fields:Array = ["file", "image"]):
	
	var entry_info : Dictionary = DataLoader.data[old_data_type][old_data_subtype][old_data_cat][old_entry_name]
	
	self.data_type     = old_data_type
	self.data_subtype  = old_data_subtype
	self.data_cat      = old_data_cat
	self.entry_name    = old_entry_name
	self.hidden_fields = fields
	self.overwrite     = true
	
	# init at original category
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.clear()
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.add_item(self.data_cat)
	
	# fill in the rest
	
	var cat_list = []
	
	if self.data_subtype == null:
		cat_list = DataLoader.data[self.data_type].keys()
		$PanelContainer/MarginContainer/VBoxContainer/Label.text = "Edit %s Data" % self.data_type
	else:
		cat_list = DataLoader.data[self.data_type][self.data_subtype].keys()
		$PanelContainer/MarginContainer/VBoxContainer/Label.text = "Edit %s %s Data" % [self.data_subtype, self.data_type]
	
	for i in cat_list.size():
		if cat_list[i] != self.data_cat:
			$PanelContainer/MarginContainer/VBoxContainer/GridContainer/CategoryEdit.add_item(cat_list[i], i)
	
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.text = self.entry_name
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/NameEdit.grab_focus()
	
	self.file_path  = entry_info.get("file", "")
	self.image_path = entry_info.get("image", "")
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/FileEdit.text = self.file_path
	$PanelContainer/MarginContainer/VBoxContainer/GridContainer/ImageEdit.select_img_file(self.image_path)
	
	hide_fields()
	self.show()
