extends HBoxContainer


var sheet_type : String = ""
var sheet_cat  : String = ""
var sheet_name : String = ""
var sheet_data : Dictionary = {}

signal sheet_closed
signal sheet_deleted


func _ready():
	
	$VBoxContainer/HBoxContainer/SaveSheet.pressed.connect(
		func () :
			DataLoader.add_entry(
				"Sheets",
				self.sheet_type,
				self.sheet_cat,
				self.sheet_name,
				self.sheet_data,
				true))
	$VBoxContainer/HBoxContainer/CloseSheet.pressed.connect(
		func () :
			self.hide()
			sheet_closed.emit())
	$VBoxContainer/DeleteSheet.pressed.connect($ConfirmDelete.show)
	$ConfirmDelete.confirmed.connect(send_sheet_deleted)


func send_sheet_deleted():
	
	DataLoader.delete_entry(
		"Sheets",
		self.sheet_type,
		self.sheet_cat,
		self.sheet_name)
	
	sheet_type = ""
	sheet_cat  = ""
	sheet_name = ""
	sheet_data = {}
	
	self.hide()
	
	sheet_deleted.emit()


func update():
	
	$VBoxContainer/TokenEdit.select_img_file(self.sheet_data.get("image", ""))
	$VBoxContainer/NameEdit.text = self.sheet_name
