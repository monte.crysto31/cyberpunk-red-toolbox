extends VBoxContainer


@export var char_type : String = "Players"

@onready var sheet_entry = preload("res://menus/character_sheets/sheet_entry.tscn").instantiate()
@onready var ui_theme = preload("res://assets/resources/main_theme.tres")

signal show_sheet(
	sheet_type:String,
	sheet_cat:String,
	sheet_name:String)

func emit_show_sheet(
	sheet_type:String,
	sheet_cat:String,
	sheet_name:String
):
	
	show_sheet.emit(
		sheet_type,
		sheet_cat,
		sheet_name)

func add_category(cat_name:String):
	
	DataLoader.add_category(
		"Sheets",
		char_type,
		cat_name)
	
	$AddCategory.hide()
	update()

func delete_category(cat_name:String):
	
	DataLoader.delete_category(
		"Sheets",
		char_type,
		cat_name)
	
	update()

func add_sheet(
	cat_name:String,
	sheet_name:String,
):
	
	DataLoader.add_entry(
		"Sheets",
		char_type,
		cat_name,
		sheet_name,
		{})
	
	update()

func delete_sheet(
	cat_name:String,
	sheet_name:String
):
	
	DataLoader.delete_entry(
		"Sheets",
		char_type,
		cat_name,
		sheet_name)
	
	update()


func update():
	
	$HBoxContainer/Label.text = char_type
	
	# data has already been fetched
	
	for category in $TabContainer.get_children():
			
		$TabContainer.remove_child(category)
		category.queue_free()
	
	for cat_name in DataLoader.data["Sheets"][char_type]:
		
		var category_node = $CategoryContainer.duplicate()
		category_node.name = cat_name
		category_node.show()
		
		for sheet_name in DataLoader.data["Sheets"][char_type][cat_name].keys():
	
			var sheet_entry_inst = sheet_entry.duplicate()
			sheet_entry_inst.char_type  = char_type
			sheet_entry_inst.cat_name   = cat_name
			sheet_entry_inst.sheet_name = sheet_name
			sheet_entry_inst.change_image(DataLoader.data["Sheets"][char_type][cat_name][sheet_name].get("image", ""))
			sheet_entry_inst.sheet_selected.connect(emit_show_sheet)
			
			category_node.get_node("HFlowContainer").add_child(sheet_entry_inst)
		
		if DataLoader.data["Sheets"][char_type][cat_name].keys().size() == 0:
			
			var delete_category_button = $DeleteCategory.duplicate()
			delete_category_button.cat_name = cat_name
			delete_category_button.delete_category.connect(delete_category)
			delete_category_button.show()
			
			category_node.get_node("HFlowContainer").add_child(delete_category_button)
		
		$TabContainer.add_child(category_node)
