extends Widget


func _ready():
	
	# widget behaviour
	SignalBus.change_widget.connect(toggle)
	
	# categories management
	
	$Lists/PlayerList/HBoxContainer/AddCat.pressed.connect(
		$AddCategory.show_add_category.bind("Sheets", "Players"))
	$Lists/NPCList/HBoxContainer/AddCat.pressed.connect(
		$AddCategory.show_add_category.bind("Sheets", "NPCs"))
	$AddCategory.category_added.connect(update)
	
	$Lists/PlayerList/HBoxContainer/AddEntry.pressed.connect(
		$AddSheet.show_add_entry.bind("Sheets", "Players", ["file"]))
	$Lists/NPCList/HBoxContainer/AddEntry.pressed.connect(
		$AddSheet.show_add_entry.bind("Sheets", "NPCs", ["file"]))
	$AddSheet.entry_added.connect(update)
	
	$Lists/PlayerList.show_sheet.connect(show_sheet)
	$Lists/NPCList.show_sheet.connect(show_sheet)
	
	# sheet display
	
	$CharacterDisplay.sheet_closed.connect(
		func () :
			$Lists.show()
			update())
	$CharacterDisplay.sheet_deleted.connect(
		func () :
			$Lists.show()
			update())


func show_sheet(char_type:String, cat_name:String, sheet_name:String):
	
	$Lists.hide()
	
	$CharacterDisplay.sheet_type = char_type
	$CharacterDisplay.sheet_cat  = cat_name
	$CharacterDisplay.sheet_name = sheet_name
	$CharacterDisplay.sheet_data = DataLoader.data["Sheets"][char_type][cat_name][sheet_name].duplicate()
	
	$CharacterDisplay.show()
	$CharacterDisplay.update()


func update():
	
	DataLoader.load_section("Sheets")
	
	$Lists/PlayerList.update()
	$Lists/NPCList.update()
