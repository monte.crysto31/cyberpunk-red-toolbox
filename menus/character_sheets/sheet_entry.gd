extends Control


var char_type   : String = ""
var cat_name    : String = ""
var sheet_name  : String = ""

signal sheet_selected(
	char_type:String,
	cat_name:String,
	sheet_name:String)

signal delete_sheet(
	char_type:String,
	cat_name:String,
	sheet_name:String)


func _ready():
	
	$VBoxContainer/Label.text = sheet_name
	
	$VBoxContainer/Button.mouse_entered.connect(
		func () : $VBoxContainer/Label.add_theme_color_override("font_color", Color("80e4ff")))
	$VBoxContainer/Button.mouse_exited.connect(
		func () : $VBoxContainer/Label.add_theme_color_override("font_color", Color("ff413d")))
	$VBoxContainer/Button.pressed.connect(send_sheet_selected)


func change_image(path:String):
	
	if path != "":
		$VBoxContainer/Button.texture_normal = ImageTexture.create_from_image(Image.load_from_file(path))


func send_sheet_selected():
	
	sheet_selected.emit(
		self.char_type,
		self.cat_name,
		self.sheet_name)


func send_delete_sheet():
	
	delete_sheet.emit(
		self.char_type,
		self.cat_name,
		self.sheet_name)
