# custom button to select an image and display it
# will return the image path when picked, "" when no selection is made

class_name ImagePicker
extends MarginContainer

signal image_selected(path:String)


func _ready():
	
	$Button.connect("pressed", $FileDialog.show)
	$FileDialog.connect("file_selected", select_img_file)


func select_img_file(path: String):
	
	if path != "":
		$MarginContainer/Image.texture = ImageTexture.create_from_image(Image.load_from_file(path))
	else:
		$MarginContainer/Image.texture = null
	
	image_selected.emit(path)
