extends MarginContainer


var sound_type : String
var cat_name   : String

signal delete_category(sound_type: String, empty_cat_name: String)


func _ready():
	
	$Delete.connect("pressed", send_delete_category)


func send_delete_category():
	
	delete_category.emit(sound_type, cat_name)
