extends Widget


@onready var music_list   = $VBoxContainer/SoundCategories/Music/MusicTabs
@onready var ambiant_list = $VBoxContainer/SoundCategories/Ambiant/AmbiantTabs
@onready var sfx_list     = $VBoxContainer/SoundCategories/SFX/SFXTabs

@onready var ui_theme    = preload("res://assets/resources/main_theme.tres")
@onready var sound_entry = preload("res://menus/jukebox/sound_entry.tscn")

var sound_types = ["Music", "Ambiant", "SFX"]


func _ready():
	
	# widget behaviour
	SignalBus.connect("change_widget", toggle)
	
	$VBoxContainer/SoundCategories/Music/HBoxContainer/AddMusic.pressed.connect(
		$AddSound.show_add_entry.bind("Sounds", "Music"))
	$VBoxContainer/SoundCategories/Ambiant/HBoxContainer/AddAmbiant.pressed.connect(
		$AddSound.show_add_entry.bind("Sounds", "Ambiant"))
	$VBoxContainer/SoundCategories/SFX/HBoxContainer/AddSFX.pressed.connect(
		$AddSound.show_add_entry.bind("Sounds", "SFX"))
	
	$VBoxContainer/SoundCategories/Music/HBoxContainer/AddMusicCat.pressed.connect(
		$AddCategory.show_add_category.bind("Sounds", "Music"))
	$VBoxContainer/SoundCategories/Ambiant/HBoxContainer/AddAmbiantCat.pressed.connect(
		$AddCategory.show_add_category.bind("Sounds", "Ambiant"))
	$VBoxContainer/SoundCategories/SFX/HBoxContainer/AddSFXCat.pressed.connect(
		$AddCategory.show_add_category.bind("Sounds", "SFX"))
	
	$AddCategory.category_added.connect(update)
	$AddSound   .entry_added   .connect(update)


func delete_category(sound_type: String, cat_name: String):
	
	DataLoader.delete_category("Sounds", sound_type, cat_name)
	
	update()

func delete_sound(
	sound_type: String,
	cat_name  : String,
	sound_name: String):
	
	DataLoader.delete_entry(
		"Sounds",
		sound_type,
		cat_name,
		sound_name)
	
	update()


func update():
	
	DataLoader.load_section("Sounds")
	
	for sound_type_node in [music_list, ambiant_list, sfx_list]:
		for category in sound_type_node.get_children():
			
			sound_type_node.remove_child(category)
			category.queue_free()
	
	var i = 0
	
	for sound_type_node in [music_list, ambiant_list, sfx_list]:
		for cat_name in DataLoader.data["Sounds"][sound_types[i]].keys():
			
			var category_node   = VBoxContainer.new()
			category_node.theme = ui_theme
			category_node.name  = cat_name
			
			if DataLoader.data["Sounds"][sound_types[i]][cat_name].keys().size() > 0:
			
				for sound_name in DataLoader.data["Sounds"][sound_types[i]][cat_name].keys():
			
					var sound_entry_inst = sound_entry.instantiate()
					sound_entry_inst.sound_type = sound_types[i]
					sound_entry_inst.cat_name   = cat_name
					sound_entry_inst.sound_name = sound_name
					sound_entry_inst.edit_sound.connect($AddSound.show_edit_entry)
					sound_entry_inst.delete_sound.connect(delete_sound)
					
					category_node.add_child(sound_entry_inst)
			
			else:
				
				var delete_category_button = $DeleteCategory.duplicate()
				delete_category_button.sound_type = sound_types[i]
				delete_category_button.cat_name   = cat_name
				delete_category_button.connect("delete_category", delete_category)
				delete_category_button.show()
				
				category_node.add_child(delete_category_button)
			
			sound_type_node.add_child(category_node)
			
		i += 1
