extends MarginContainer


var sound_type : String
var cat_name   : String
var sound_name : String

signal edit_sound(
	sound_type : String,
	cat_name   : String,
	sound_name : String)

signal delete_sound(
	sound_type: String,
	cat_name  : String,
	sound_name: String)


func _ready():
	
	$HBoxContainer/Button.text = sound_name
	$HBoxContainer/Button.connect("pressed", send_play_sound)
	$HBoxContainer/Edit  .connect("pressed", send_edit_sound)
	$HBoxContainer/Delete.connect("pressed", send_delete_sound)


func send_play_sound():
	
	match sound_type:
		
		"Music":
			SignalBus.play_music.emit(
				self.cat_name,
				self.sound_name)
		
		"Ambiant":
			SignalBus.play_ambiant.emit(
				self.cat_name,
				self.sound_name)
		
		"SFX":
			SignalBus.play_sfx.emit(
				self.cat_name,
				self.sound_name)

func send_edit_sound():
	
	edit_sound.emit(
		"Sounds",
		self.sound_type,
		self.cat_name,
		self.sound_name,
		[])

func send_delete_sound():
	
	delete_sound.emit(
		self.sound_type,
		self.cat_name,
		self.sound_name)
