extends Control


var scroll_speed = 64.0


func _ready():
	
	get_tree().root.add_child.call_deferred(preload("res://menus/player_view/player_view.tscn").instantiate())


func _process(delta):
	
	$Background.scroll_offset.y -= scroll_speed * delta
