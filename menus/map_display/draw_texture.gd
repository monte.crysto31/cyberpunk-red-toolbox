extends TextureRect

@export var scroll_area : ScrollContainer
@export_enum(
	"Free Draw",
	"Straight Line",
	"Rect Area",
	"Lasso") var draw_mode = 0

@onready var pencil_preview : Panel  = $Control/Pencil
@onready var line_preview   : Line2D = $Control/Line

var layer : MapLayer = MapLayer.new()
var drawing_area : Rect2i

var pencil_size  : int    = 12
var pencil_brush : Rect2i = Rect2i(Vector2i.ZERO, Vector2i.ONE * pencil_size)
var pencil_color : Color  = Color(1., 1., 1., 1.)

var draw_start_pos : Variant = null
var draw_end_pos   : Variant = null

var should_update_canvas : bool = false
var drawing              : bool = false
var active               : bool = true

signal edit_fow(image:Image)


func _process(_delta):
	
	self.active = Input.is_key_pressed(KEY_SHIFT)
	
	if self.active:
		self.mouse_filter = Control.MOUSE_FILTER_PASS
	else:
		self.mouse_filter = Control.MOUSE_FILTER_IGNORE
	
	if should_update_canvas:
		update_texture()


func pos_to_local(pos:Vector2) -> Vector2i:
	
	return Vector2i((pos - self.global_position) / self.layer.zoom / (self.layer.cell_size / self.layer.fow_ppc))


# Called when the node enters the scene tree for the first time.
func init(new_layer:MapLayer, rect:Rect2i, keep_fow:bool):
	
	var tmp_fow : Image = null
	if keep_fow:
		tmp_fow = self.layer.fow.duplicate()
	
	self.layer = new_layer.duplicate()
	self.drawing_area = rect
	
	self.pencil_brush.size   = Vector2i.ONE * self.pencil_size
	self.pencil_preview.size = self.pencil_brush.size * self.layer.zoom * self.layer.cell_size / self.layer.fow_ppc
	
	if not keep_fow:
		update_texture()
	else:
		self.layer.fow = tmp_fow


# width in ratio of cell size
func resize_pencil(new_width:float):
	
	self.pencil_size         = floori(new_width * float(self.layer.fow_ppc))
	self.pencil_brush.size   = Vector2i.ONE * self.pencil_size
	self.pencil_preview.size = self.pencil_brush.size * self.layer.zoom * self.layer.cell_size / self.layer.fow_ppc


func move_pencil(pen_pos:Vector2):
	
	var local_position = pos_to_local(pen_pos)
	
	pencil_brush.position = local_position - pencil_brush.size / 2
	pencil_preview.global_position = pen_pos - pencil_preview.size / 2.0


# walk the pencil from start to end position
func walk_pencil():
	
	pencil_preview.hide()
	move_pencil(draw_start_pos)
	
	var local_start:Vector2i = pos_to_local(draw_start_pos)
	var line:Vector2 = Vector2(pos_to_local(draw_end_pos) - local_start)
	
	if line.x == 0.:
		line.x = 1.
	if line.y == 0.:
		line.y = 1.
	
	var line_length:Vector2 = line.abs()
	var steps = ceili(maxf(line_length.x, line_length.y))
	
	for i in steps:
		
		pencil_brush.position = local_start + Vector2i(line * float(i) / float(steps)) - pencil_brush.size / 2
		self.layer.fow.fill_rect(pencil_brush, self.pencil_color)


func update_texture():
	
	self.texture = ImageTexture.create_from_image(self.layer.fow)
	should_update_canvas = false


func _input(event):
	
	if not self.active:
		return
	
	if event is InputEventMouseButton:
		
		match event.button_index:
			
			MOUSE_BUTTON_LEFT:
				self.pencil_color = Color(1., 1., 1., 1.)
			
			MOUSE_BUTTON_RIGHT:
				self.pencil_color = Color(0., 0., 0., 1.)
			
			_:
				return
		
		#if we weren't drawing and are pressing now, then we just pressed
		if (not drawing
			and event.pressed
			and drawing_area.has_point(event.global_position)
		):
			
			var tmp_mouse = get_global_mouse_position()
			draw_start_pos = tmp_mouse
			draw_end_pos   = tmp_mouse
			
			drawing = true
		
		# here we just released the mouse after drawing
		if (drawing
			and not event.pressed
			and self.layer.fow != null
		):
			
			if draw_start_pos != null:
				match draw_mode:
					
					1:
						draw_end_pos = get_global_mouse_position()
						walk_pencil()
						should_update_canvas = true
					
					2:
						self.layer.fow.fill_rect(
							Rect2i(
								pos_to_local(draw_start_pos),
								pos_to_local(draw_end_pos) \
								 - pos_to_local(draw_start_pos) \
								 + Vector2i.ONE),
							self.pencil_color)
						should_update_canvas = true
			
			pencil_preview.hide()
			line_preview.hide()
			draw_start_pos = null
			draw_end_pos   = null
			
			drawing = false
			
			edit_fow.emit(self.layer.fow)
		
	if (event is InputEventMouseMotion
		and drawing
		and self.layer.fow != null
		and draw_start_pos != null
	):
		
		draw_end_pos = get_global_mouse_position() \
			.clamp(drawing_area.position, drawing_area.position + drawing_area.size)
		
		line_preview.set_point_position(0,
			draw_start_pos \
			 - Vector2(drawing_area.position) \
			 + Vector2(scroll_area.scroll_horizontal, scroll_area.scroll_vertical))
		line_preview.set_point_position(1,
			draw_end_pos \
			 - Vector2(drawing_area.position) \
			 + Vector2(scroll_area.scroll_horizontal, scroll_area.scroll_vertical))
		
		match draw_mode:
			
			# free draw
			0:
				walk_pencil()
				draw_start_pos = draw_end_pos
				
				move_pencil(draw_end_pos)
				pencil_preview.show()
				
				should_update_canvas = true
			
			# straight line
			1:
				move_pencil(draw_end_pos)
				pencil_preview.show()
				line_preview.show()
			
			# rect area
			2:
				pencil_preview.global_position = Vector2(
					min(draw_start_pos.x, draw_end_pos.x),
					min(draw_start_pos.y, draw_end_pos.y))
				pencil_preview.size = abs(draw_end_pos - draw_start_pos)
				pencil_preview.show()
			
			# lasso
			3:
				walk_pencil()
				
				move_pencil(draw_end_pos)
				pencil_preview.show()
				line_preview.show()
				
				should_update_canvas = true
