extends ColorRect


var layer_names = []

signal delete_layer(index:int)
signal edit_layers(layer_names:Array)


func _ready():
	
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Cancel.connect("pressed", self.hide)
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Done.connect("pressed", send_edit_layers)


func update_names(index:int, new_name:String):
	
	self.layer_names[index] = new_name

func send_edit_layers():
	
	edit_layers.emit(self.layer_names)
	self.hide()


func show_edit_layers(layers:Array[MapLayer]):
	
	var layers_list = $PanelContainer/MarginContainer/VBoxContainer/Layers
	for node in layers_list.get_children():
		layers_list.remove_child(node)
		node.queue_free()
	
	layer_names = []
	
	for i in (layers.size()+1):
		
		# add one field to add a new layer
		if i < layers.size():
			layer_names.push_back(layers[i].name)
		else:
			layer_names.push_back("")
		
		var layer_label = $LayerEdit/Label    .duplicate()
		var layer_edit  = $LayerEdit/LayerName.duplicate()
		
		layer_label.text = str(i)
		layer_edit .text = layer_names[i]
		
		layer_edit.text_changed.connect(
			func (new_text:String) :
				update_names(i, new_text))
		
		layer_label.show()
		layer_edit.show()
		
		layers_list.add_child(layer_label)
		layers_list.add_child(layer_edit)
		
		# add delete button only for existing layers
		if i < layers.size():
			var layer_delete = $LayerEdit/LayerDelete.duplicate()
			layer_delete.pressed.connect(
				func ():
					delete_layer.emit(i))
			layer_delete.show()
			layers_list.add_child(layer_delete)
	
	self.show()
