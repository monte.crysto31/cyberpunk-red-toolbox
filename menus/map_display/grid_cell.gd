extends PanelContainer


@onready var default_style = preload("res://assets/resources/empty_cell.tres")

var grid_coordinates = - Vector2i.ONE

var token_info : Variant     = null
var tex        : TextureRect = TextureRect.new()


signal set_cell(coords:Vector2i, data:Variant)


func update_cell(old_coords:Vector2i, new_info:Variant, new_tex:TextureRect):
	
	if old_coords != - Vector2i.ONE and Globals.player_view_state == Globals.MAPS:
		SignalBus.move_remote_token.emit(old_coords, self.grid_coordinates, new_tex.texture)
	
	self.token_info   = new_info
	self.tooltip_text = token_info["name"]
	
	self.tex         = new_tex
	self.tex.size    = Vector2(1., 1.)
	self.tex.texture = ImageTexture.create_from_image(Image.load_from_file(token_info["image"]))
	
	var style_box = StyleBoxTexture.new()
	style_box.texture = tex.texture
	self.add_theme_stylebox_override("panel", style_box)
	
	self.custom_minimum_size = self.tex.custom_minimum_size
	self.size                = Vector2.ONE


func _get_drag_data(_pos):
	
	if token_info == null or token_info.get("image") == null:
		return null
	
	var tmp = token_info.duplicate()
	var tmp_tex = tex.duplicate()
	
	self.set_cell.emit(self.grid_coordinates, null)
	
	tmp_tex.custom_minimum_size = self.custom_minimum_size
	tmp_tex.size = Vector2.ONE
	
	token_info = {}
	tex.texture = null
	self.tooltip_text = ""
	
	var preview = Control.new()
	preview.add_child(tmp_tex)
	set_drag_preview(preview)
	tmp_tex.position = -0.5 * tmp_tex.size
	
	self.add_theme_stylebox_override("panel", default_style)
	
	return [self.grid_coordinates, tmp, tmp_tex]


func _can_drop_data(_pos, data):
	
	return (typeof(data) == TYPE_ARRAY and
		data[1] != null and
		data[1].get("type") == "Sheets")


func _drop_data(_pos, data):
	
	update_cell(data[0], data[1].duplicate(), data[2].duplicate())
	
	self.set_cell.emit(self.grid_coordinates, self.token_info)
