extends Control


@onready var grid_scroll : ScrollContainer = $HBoxContainer/VBoxContainer/PanelContainer/Grid
@onready var draw_fow    : TextureRect     = $HBoxContainer/VBoxContainer/PanelContainer/Grid/PanelContainer/DrawFOW
@onready var grid_map    : GridContainer   = $HBoxContainer/VBoxContainer/PanelContainer/Grid/PanelContainer/GridContainer

@onready var token_entry = preload("res://menus/map_display/token_entry.tscn").instantiate()

var threads : Array[Thread] = []

var map = Map.new()
var tmp_fow : Image = null

signal map_closed
signal map_deleted


func _ready():
	
	for i in range(Globals.nb_threads):
		self.threads.push_back(Thread.new())
	
	# map controls
	
	$HBoxContainer/MapControls/Controls1/NameEdit.text_changed.connect(
		func (new_name:String):
			map.name = new_name)
	
	$HBoxContainer/MapControls/Controls1/HBoxContainer/ChangeLayer.item_selected.connect(
		func (index:int):
			map.switch_layer(index)
			update())
	$HBoxContainer/MapControls/Controls1/HBoxContainer/EditLayer.pressed.connect(
		func ():
			$EditLayers.show_edit_layers(map.layers))
	
	$HBoxContainer/MapControls/GridControls/MapBGEdit.pressed.connect($FileDialog.show)
	$FileDialog.file_selected.connect(
		func (path:String):
			map.set_layer_background(path)
			update_controls()
			update_grid())
	
	$EditLayers.delete_layer.connect(
		func (index:int):
			map.delete_layer(index)
			update_controls()
			$EditLayers.show_edit_layers(map.layers))
	$EditLayers.edit_layers.connect(
		func (layer_names:Array):
			for i in layer_names.size():
				# existing layers will be updated
				if i < (layer_names.size()-1):
					map.rename_layer(i, layer_names[i])
				# the new one will be created only if its name isn't empty
				elif layer_names[i] != "":
					map.add_layer(layer_names[i])
			update())
	
	$HBoxContainer/MapControls/GridControls/HBoxContainer/MapSizeX.value_changed.connect(
		func (new_value:int): 
			map.resize_layer(Vector2i(new_value, map.get_size().y))
			update_grid()
			update_draw_area())
	$HBoxContainer/MapControls/GridControls/HBoxContainer/MapSizeY.value_changed.connect(
		func (new_value:int): 
			map.resize_layer(Vector2i(map.get_size().x, new_value))
			update_grid()
			update_draw_area())
	$HBoxContainer/MapControls/GridControls/ZoomEdit.value_changed.connect(
		func (new_zoom:float):
			var old_zoom = map.get_zoom()
			map.set_zoom(new_zoom)
			update_controls()
			resize_grid(old_zoom)
			update_draw_area(true))
	
	$HBoxContainer/MapControls/ShowPlayerView.pressed.connect(
		func ():
			SignalBus.show_remote_map.emit(map.duplicate()))
	
	# data loading
	
	$HBoxContainer/MapControls/VBoxContainer/HBoxContainer/Save.pressed.connect(
		func ():
			DataLoader.add_entry(
				"Maps",
				null,
				map.category,
				map.name,
				map.serialize(),
				true))
	$HBoxContainer/MapControls/VBoxContainer/HBoxContainer/Close.pressed.connect(emit_signal.bind("map_closed"))
	$HBoxContainer/MapControls/VBoxContainer/Delete.pressed.connect($ConfirmDelete.show)
	$ConfirmDelete.confirmed.connect(
		func ():
			DataLoader.delete_entry(
				"Maps",
				null,
				map.category,
				map.name)
			map_deleted.emit()
			self.hide())
	
	# grid editing and toolbar
	
	$HBoxContainer/VBoxContainer/HBoxContainer/DrawMode.item_selected.connect(
		func (index:int):
			draw_fow.draw_mode = index)
	
	$HBoxContainer/VBoxContainer/HBoxContainer/DrawWidth.value_changed.connect(
		draw_fow.resize_pencil)
	
	$HBoxContainer/VBoxContainer/HBoxContainer/CommitFOW.pressed.connect(
		func ():
			map.set_fow(tmp_fow)
			$HBoxContainer/VBoxContainer/HBoxContainer/CommitFOW.disabled = true
			$HBoxContainer/VBoxContainer/HBoxContainer/UndoFOW  .disabled = true
			if Globals.player_view_state == Globals.MAPS:
				SignalBus.refresh_remote_fow.emit(map.get_fow()))
	$HBoxContainer/VBoxContainer/HBoxContainer/UndoFOW.pressed.connect(
		func ():
			tmp_fow = map.get_fow()
			update_draw_area()
			if Globals.player_view_state == Globals.MAPS:
				SignalBus.refresh_remote_fow.emit(map.get_fow()))
	
	draw_fow.edit_fow.connect(
		func (new_fow:Image):
			tmp_fow = new_fow.duplicate()
			if Globals.player_view_state == Globals.MAPS:
				$HBoxContainer/VBoxContainer/HBoxContainer/CommitFOW.disabled = false
				$HBoxContainer/VBoxContainer/HBoxContainer/UndoFOW.  disabled = false)


func load_map(data:Dictionary):
	
	draw_fow.set_visible(true)
	
	map.deserialize(data)
	
	update()


func get_map_global_rect() -> Rect2:
	
	var map_rect : Rect2 = Rect2(
		$HBoxContainer/VBoxContainer/PanelContainer/Grid/PanelContainer.global_position,
		map.get_size() * map.get_cell_size() * map.get_zoom())
	
	return map_rect


func update_controls():
	
	# general info
	
	$HBoxContainer/MapControls/Controls1/NameEdit.text = map.name
	
	$HBoxContainer/MapControls/Controls1/HBoxContainer/ChangeLayer.clear()
	for i in map.layers.size():
		$HBoxContainer/MapControls/Controls1/HBoxContainer/ChangeLayer.add_item(map.layers[i].name, i)
	$HBoxContainer/MapControls/Controls1/HBoxContainer/ChangeLayer.select(map.curr_layer)
	
	# layer info
	
	var bg_path : String = map.layers[map.curr_layer].background
	
	if bg_path == "":
		$HBoxContainer/MapControls/GridControls/MapBGEdit.text = "Select a file..."
	else:
		$HBoxContainer/MapControls/GridControls/MapBGEdit.text = bg_path.split("/")[-1]
	
	$HBoxContainer/MapControls/GridControls/HBoxContainer/MapSizeX.set_value_no_signal(map.get_size().x)
	$HBoxContainer/MapControls/GridControls/HBoxContainer/MapSizeY.set_value_no_signal(map.get_size().y)
	$HBoxContainer/MapControls/GridControls/ZoomEdit.set_value_no_signal(map.get_zoom())


func update_tokens():
	
	DataLoader.load_section("Sheets")
	
	var token_lists = {
		"Players" : $HBoxContainer/MapControls/TabContainer/Players/PlayerTokens,
		"NPCs"    : $HBoxContainer/MapControls/TabContainer/NPCs/NPCTokens
	}
	
	for token_subtype in ["Players", "NPCs"]:
		
		for node in token_lists[token_subtype].get_children():
			token_lists[token_subtype].remove_child(node)
			node.queue_free()
		
		var token_cats = DataLoader.data["Sheets"][token_subtype].keys()
		
		for token_cat in token_cats:
			
			$TokenSeparator/Label.text = token_cat
			var token_separator = $TokenSeparator.duplicate()
			token_separator.show()
			
			token_lists[token_subtype].add_child(token_separator)
			
			for token_name in DataLoader.data["Sheets"][token_subtype][token_cat]:
				
				var token_entry_inst = token_entry.duplicate()
				token_entry_inst.token_info = DataLoader.data["Sheets"][token_subtype][token_cat][token_name]
				token_entry_inst.update()
				
				token_lists[token_subtype].add_child(token_entry_inst)


func _thread_resize_cells(cells:Array[Node], new_size:Vector2):
	
	for cell in cells:
		
		cell.tex.set_custom_minimum_size.call_deferred(new_size)
		cell    .set_custom_minimum_size.call_deferred(new_size)
		cell.tex.set_size.call_deferred(Vector2.ONE)
		cell    .set_size.call_deferred(Vector2.ONE)


func resize_grid(old_zoom:float):
	
	var new_zoom  : float       = map.get_zoom()
	var new_size : Vector2     = Vector2.ONE * map.get_cell_size() * new_zoom
	var cells     : Array[Node] = self.grid_map.get_children()
	
	for th in Globals.nb_threads:
		self.threads[th].start(_thread_resize_cells.bind(
			cells.slice(
				floori(float(th)   * float(cells.size()) / float(Globals.nb_threads)),
				ceili (float(th+1) * float(cells.size()) / float(Globals.nb_threads))),
			new_size))
	
	for th in Globals.nb_threads:
		self.threads[th].wait_to_finish()
	
	await get_tree().process_frame
	
	var scroll_rect : Rect2 = self.grid_scroll.get_global_rect()
	var grid_rect   : Rect2 = get_map_global_rect()
	
	# target point : if we are showing the zoomgrabber, then it's the mouse,
	# and the center of the visible area of the grid if not
	var target : Vector2
	if $HBoxContainer/VBoxContainer/PanelContainer/ZoomGrabber.visible:
		target = get_global_mouse_position()
	else:
		target = scroll_rect.position + scroll_rect.size/2
	
	# trust me bro I had to draw to find this shit
	var offset : Vector2 = (target - grid_rect.position) * (old_zoom/new_zoom - 1.)
	
	self.grid_scroll.get_h_scroll_bar().value -= offset.x
	self.grid_scroll.get_v_scroll_bar().value -= offset.y


func resize_tokens():
	
	# we don't question the zoom, it's the right one
	
	var map_size  : Vector2i = map.get_size()
	var cell_size : float    = map.get_cell_size()
	var map_zoom  : float    = map.get_zoom()
	
	for coords in map.get_tokens().keys():
		
		var grid_index = coords.x * map_size.x + coords.y
		
		self.grid_map.get_child(grid_index).tex.custom_minimum_size = Vector2(cell_size, cell_size) * map_zoom
		self.grid_map.get_child(grid_index).custom_minimum_size     = Vector2(cell_size, cell_size) * map_zoom
		self.grid_map.get_child(grid_index).tex.size = Vector2.ONE
		self.grid_map.get_child(grid_index).size     = Vector2.ONE


func update_grid():
	
	# change map size
	
	var map_size : Vector2i = map.get_size()
	var map_zoom  = map.get_zoom()
	var cell_size = map.get_cell_size()
	
	self.grid_map.set_columns(map_size.x)
	
	# map background
	
	var stylebox = StyleBoxTexture.new()
	stylebox.texture = map.get_background()
	$HBoxContainer/VBoxContainer/PanelContainer/Grid/PanelContainer.add_theme_stylebox_override("panel", stylebox)
	
	# base empty cells
	
	for node in self.grid_map.get_children():
		self.grid_map.remove_child(node)
		node.queue_free()
	
	var empty_cell = preload("res://menus/map_display/grid_cell.tscn").instantiate()
	empty_cell.custom_minimum_size = Vector2.ONE * cell_size * map_zoom
	
	for i in map_size.y:
		for j in map_size.x:
			
			var cell = empty_cell.duplicate()
			cell.grid_coordinates = Vector2i(i, j)
			
			cell.set_cell.connect(
				func (coords:Vector2i, data:Variant):
					map.set_cell(coords, data)
					if data != null:
						resize_tokens())
			
			if map.get_cell(cell.grid_coordinates) != null:
				
				cell.tex = token_entry.get_node("TextureRect").duplicate()
				cell.update_cell(
					-Vector2i.ONE,
					map.get_cell(cell.grid_coordinates),
					token_entry.get_node("TextureRect").duplicate())
			
			self.grid_map.add_child(cell)
	
	resize_tokens()


func update_draw_area(keep_fow:bool = false):
	
	# we can draw on the visible part of the grid, or the intersection of
	# the panel and the scroll area

	var drawing_rect = get_map_global_rect().intersection(self.grid_scroll.get_global_rect())
	
	self.draw_fow.drawing = false
	self.draw_fow.init(map.layers[map.curr_layer], drawing_rect, keep_fow)
	
	if not keep_fow:
		$HBoxContainer/VBoxContainer/HBoxContainer/CommitFOW.disabled = true
		$HBoxContainer/VBoxContainer/HBoxContainer/UndoFOW  .disabled = true


func update():
	
	update_controls()
	update_tokens()
	update_grid()
	update_draw_area()
