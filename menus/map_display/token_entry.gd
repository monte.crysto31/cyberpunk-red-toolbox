extends HBoxContainer


var token_info : Dictionary = {}


func _ready():
	
	$TextureRect.mouse_entered.connect(
		func () : $Label.add_theme_color_override("font_color", Color("80e4ff")))
	$TextureRect.mouse_exited.connect(
		func () : $Label.add_theme_color_override("font_color", Color("ff413d")))


func update():
	
	$TextureRect.texture = ImageTexture.create_from_image(Image.load_from_file(token_info["image"]))
	$Label.text = token_info["name"]


# Returns the data to pass from an object when you click and drag away from
# this object. Also calls set_drag_preview() to show the mouse dragging
# something so the user knows that the operation is working.

func _get_drag_data(_pos):
	
	if token_info.get("image") == null:
		return null
	
	# Use TextureRect as drag preview.
	
	var tex : TextureRect = $TextureRect.duplicate()
	tex.set_anchors_and_offsets_preset(Control.PRESET_CENTER)

	# Allows us to center the TextureRect on the mouse
	var preview = Control.new()
	preview.add_child(tex)
	tex.position = -0.5 * tex.size

	# Sets what the user will see they are dragging
	set_drag_preview(preview)

	# Return token info as data
	return [-Vector2i.ONE, token_info, tex]
