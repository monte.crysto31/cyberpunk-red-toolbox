extends Control


var map_cat  : String = ""
var map_name : String = ""

signal map_selected(
	map_cat:String,
	map_name:String)


func _ready():
	
	$VBoxContainer/Label.text = map_name
	
	$VBoxContainer/Button.mouse_entered.connect(
		func () : $VBoxContainer/Label.add_theme_color_override("font_color", Color("80e4ff")))
	$VBoxContainer/Button.mouse_exited.connect(
		func () : $VBoxContainer/Label.add_theme_color_override("font_color", Color("ff413d")))
	$VBoxContainer/Button.pressed.connect(send_map_selected)


func change_image(path:String):
	
	if path != "":
		$VBoxContainer/Button.texture_normal = ImageTexture.create_from_image(Image.load_from_file(path))


func send_map_selected():
	
	map_selected.emit(
		self.map_cat,
		self.map_name)

