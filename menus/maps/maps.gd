extends Widget


@onready var map_list : TabContainer = $MapSelection/TabContainer

@onready var ui_theme     = preload("res://assets/resources/main_theme.tres")
@onready var map_entry = preload("res://menus/maps/map_entry.tscn")


func _ready():
	
	# widget behaviour
	SignalBus.change_widget.connect(toggle)
	
	# map selection
	
	$MapSelection/HBoxContainer/AddMapCat.pressed.connect($AddCategory.show_add_category.bind(
		"Maps",
		null))
	$MapSelection/HBoxContainer/AddMap.pressed.connect($AddMap.show_add_entry.bind(
		"Maps",
		null,
		["file", "image"]))
	
	$AddCategory.category_added.connect(update)
	$AddMap     .entry_added   .connect(update)
	
	$MapDisplay.map_closed .connect(hide_map)
	$MapDisplay.map_deleted.connect(hide_map)


func delete_category(cat_name:String):
	
	DataLoader.delete_category("Maps", null, cat_name)
	update()

func add_map(cat_name:String, map_name:String):
	
	DataLoader.add_entry(
		"Maps",
		null,
		cat_name,
		map_name,
		{})
	
	update()


func show_map(map_cat:String, map_name:String):
	
	$MapDisplay.load_map(DataLoader.data["Maps"][map_cat][map_name])
	
	$MapSelection.hide()
	$MapDisplay  .show()

func hide_map():
	
	$MapDisplay.hide()
	$MapSelection.show()
	
	update()


func update():
	
	DataLoader.load_section("Maps")
	
	for category in map_list.get_children():
			
		map_list.remove_child(category)
		category.queue_free()
	
	for map_cat in DataLoader.data["Maps"].keys():
		
		var category_node  = $CategoryContainer.duplicate()
		category_node.name = map_cat
		
		for map_name in DataLoader.data["Maps"][map_cat].keys():
	
			var map_entry_inst = map_entry.instantiate()
			map_entry_inst.map_cat  = map_cat
			map_entry_inst.map_name = map_name
			map_entry_inst.map_selected.connect(show_map)
			map_entry_inst.change_image(DataLoader.data["Maps"][map_cat][map_name].get("image", ""))
			#map_entry_inst.map_deleted .connect(update)
			
			category_node.get_node("HFlowContainer").add_child(map_entry_inst)
		
		if DataLoader.data["Maps"][map_cat].keys().size() == 0:
			
			var delete_category_button = $DeleteCategory.duplicate()
			delete_category_button.cat_name = map_cat
			delete_category_button.delete_category.connect(delete_category)
			delete_category_button.show()
			
			category_node.get_node("HFlowContainer").add_child(delete_category_button)
		
		map_list.add_child(category_node)
