extends Window


var scroll_speed : float = 64.0
var map : Map = Map.new()


func _ready():
	
	self.close_requested.connect(self.hide)
	self.visibility_changed.connect(self._on_visibility_changed)
	
	SignalBus.show_remote_NET.connect(show_NET)
	
	SignalBus.show_remote_map      .connect(show_map)
	SignalBus.move_remote_map      .connect(move_map)
	SignalBus.move_remote_token    .connect(move_token)
	SignalBus.refresh_remote_fow   .connect(refresh_fow)
	SignalBus.refresh_remote_tokens.connect(refresh_tokens)
	
	clear_display()


func _process(delta):
	
	$Background.scroll_offset.y -= scroll_speed * delta


func _on_visibility_changed():
	
	Globals.is_player_view_visible = self.visible


func clear_display():
	
	$Background.show()
	
	clear_NET()
	clear_map()
	
	Globals.player_view_state = Globals._EMPTY


func show_NET(subnet_display:Control):
	
	clear_display()
	Globals.player_view_state = Globals.NET
	
	self.title = "Player View - NET"
	
	var display_copy = preload("res://menus/NET_display/subnet_display.tscn").instantiate()
	display_copy.subwindow    = true
	display_copy.curr_cell    = subnet_display.curr_cell
	display_copy.visible_cell = subnet_display.visible_cell
	display_copy.get_node("ProgramDesc").curr_program = subnet_display.get_node("ProgramDesc").curr_program
	display_copy.NET.deserialize_NET(
		subnet_display.NET.name,
		subnet_display.NET.category,
		subnet_display.NET.serialize_NET())
	display_copy.update()
	
	$Contents.add_child(display_copy)
	
	self.show()
	
	# get current cell and ensure it is visible
	display_copy.ensure_curr_cell_visible()

func clear_NET():
	
	for node in $Contents.get_children():
		$Contents.remove_child(node)
		node.queue_free()


func show_map(new_map:Map):
	
	clear_display()
	$Background.hide()
	
	Globals.player_view_state = Globals.MAPS
	
	self.map = new_map
	
	$Map.scale = Vector2.ONE * self.map.get_zoom()
	
	$Map/Background.texture = self.map.get_background()
	$Map/FoW.texture = ImageTexture.create_from_image(self.map.get_fow())
	
	$Map/Background.position            = Vector2.ZERO
	$Map/Background.custom_minimum_size = Vector2(self.map.get_size()) * self.map.get_cell_size()
	$Map/FoW       .position            = Vector2.ZERO
	$Map/FoW       .custom_minimum_size = Vector2(self.map.get_size()) * self.map.get_cell_size()
	
	refresh_tokens(self.map.get_tokens())
	
	self.show()

func clear_map():
	
	for node in $Map/Tokens.get_children():
		$Map/Tokens.remove_child(node)
		node.queue_free()
	
	self.map = Map.new()
	$Map/Background.texture = null
	$Map/FoW.texture = null

func move_map(new_pos:Vector2, new_zoom:float):
	
	if Globals.player_view_state != Globals.MAPS:
		return
	
	await get_tree().process_frame
	await get_tree().process_frame
	
	var tween : Tween = create_tween()
	tween.tween_property($Map, "global_position", - new_pos * new_zoom + Vector2(self.size) / 2., 0.3) \
		.from_current() \
		.set_trans(Tween.TRANS_SINE)
	
	var tween2 : Tween = create_tween()
	tween2.tween_property($Map, "scale", Vector2.ONE * new_zoom, 0.3) \
		.from_current() \
		.set_trans(Tween.TRANS_SINE)

func refresh_fow(new_fow:Image):
	
	if Globals.player_view_state != Globals.MAPS:
		return
	
	self.map.set_fow(new_fow)
	$Map/FoW.texture = ImageTexture.create_from_image(new_fow)

func refresh_tokens(new_tokens:Dictionary):
	
	if Globals.player_view_state != Globals.MAPS:
		return
	
	self.map.layers[self.map.curr_layer].new_tokens = new_tokens
	
	for node in $Map/Tokens.get_children():
		$Map/Tokens.remove_child(node)
		node.queue_free()
	
	for coords in new_tokens:
		
		var token : TextureRect = $Map/Token.duplicate()
		$Map/Tokens.add_child(token)
		
		# all sheets have at least the default image in their data
		token.texture = ImageTexture.create_from_image(Image.load_from_file(new_tokens[coords]["image"]))
		token.position = Vector2(coords.y, coords.x) * self.map.get_cell_size()
		token.custom_minimum_size = Vector2.ONE * self.map.get_cell_size()
		token.size                = Vector2.ONE
		token.name = str(coords)
		
		token.show()

func move_token(old_coords:Vector2i, new_coords:Vector2i, token_texture:ImageTexture):
	
	if Globals.player_view_state != Globals.MAPS:
		return
	
	var token : TextureRect = $Map/Tokens.get_node(str(old_coords))
	
	token.texture  = token_texture
	token.position = Vector2(old_coords.y, old_coords.x) * self.map.get_cell_size()
	token.name     = str(new_coords)
	
	var tween : Tween = create_tween()
	tween.tween_property(token, "position", Vector2(new_coords.y, new_coords.x) * self.map.get_cell_size(), 0.3) \
		.from_current() \
		.set_trans(Tween.TRANS_SINE)
	
