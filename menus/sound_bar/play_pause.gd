extends TextureButton


@export var AudioPlayer : AudioStreamPlayer
@export var Progress    : HSlider
@export_enum("Play", "Pause") var state = 0

@onready var textures : Dictionary = {
	0 : [
		preload("res://assets/gui/button/start_idle.png"),
		preload("res://assets/gui/button/start_hover.png"),
	],
	1 : [
		preload("res://assets/gui/button/stop_idle.png"),
		preload("res://assets/gui/button/stop_hover.png"),
	]
}


func _ready():
	
	connect("pressed", on_button_pressed)
	update_textures()


func set_state(new_state: int):
	
	state = new_state
	update_textures()


func on_button_pressed():
	
	if AudioPlayer.stream != null:
		
		match state:
			
			0:
				AudioPlayer.play(Progress.value * AudioPlayer.stream.get_length())
			
			1:
				var last_position = AudioPlayer.get_playback_position()
				AudioPlayer.stop()
				AudioPlayer.seek(last_position)
		
		state = 1 - state
		update_textures()


func update_textures():
	
	texture_normal = textures[state][0]
	texture_hover  = textures[state][1]
