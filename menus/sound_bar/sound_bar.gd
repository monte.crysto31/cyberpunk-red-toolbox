extends MarginContainer


@onready var music_progress   : HSlider = $HBoxContainer/VBoxContainer/MusicProgress
@onready var ambiant_progress : HSlider = $HBoxContainer/VBoxContainer2/AmbiantProgress

@onready var default_cover : Texture2D = preload("res://icon.png")

var curr_music_name : String     = ""
var curr_music_info : Dictionary = {}
var dragging_music  : bool = false

var curr_ambiant_name : String     = ""
var curr_ambiant_info : Dictionary = {}
var dragging_ambiant  : bool = false

var curr_sfx_name : String     = ""
var curr_sfx_info : Dictionary = {}


func _ready():
	
	SignalBus.connect("play_music",   play_music)
	SignalBus.connect("play_ambiant", play_ambiant)
	SignalBus.connect("play_sfx",     play_sfx)
	
	music_progress  .connect("drag_started", func ():         dragging_music   = true)
	music_progress  .connect("drag_ended",   func (_changed): dragging_music   = false)
	ambiant_progress.connect("drag_started", func ():         dragging_ambiant = true)
	ambiant_progress.connect("drag_ended",   func (_changed): dragging_ambiant = false)


func _process(_delta):
	
	if $MusicPlayer.stream != null:
		
		if dragging_music:
			$MusicPlayer.seek(music_progress.value * $MusicPlayer.stream.get_length())
		elif $MusicPlayer.is_playing():
			music_progress.value = $MusicPlayer.get_playback_position() / $MusicPlayer.stream.get_length()
	
	if $AmbiantPlayer.stream != null:
		
		if dragging_ambiant:
			$AmbiantPlayer.seek(ambiant_progress.value * $AmbiantPlayer.stream.get_length())
		elif $AmbiantPlayer.is_playing():
			ambiant_progress.value = $AmbiantPlayer.get_playback_position() / $AmbiantPlayer.stream.get_length()


func set_stream_loop(stream, loop: bool):
	
	# OGG and MP3 streams have a loop property
	if stream is AudioStreamOggVorbis or stream is AudioStreamMP3:
		stream.loop = loop
	
	# WAV has a loop_mode
	else:
		stream.loop_mode = int(loop)


func play_music(category: String, sound_name: String):
	
	$HBoxContainer/VBoxContainer/MusicName.text = "%s: %s" % [category, sound_name]
	curr_music_name = sound_name
	curr_music_info = DataLoader.get_entry_data("Sounds", "Music", category, sound_name)
	
	if curr_music_info["image"] != "":
		$HBoxContainer/TextureRect.texture = \
			ImageTexture.create_from_image(Image.load_from_file(curr_music_info["image"]))
	else:
		$HBoxContainer/TextureRect.texture = default_cover
	
	$MusicPlayer.set_stream(AudioFileLoader.loadfile(curr_music_info["file"]))
	$MusicPlayer.play()
	
	$HBoxContainer/VBoxContainer/MusicControls/PlayPause.set_state(1)


func play_ambiant(category: String, sound_name: String):
	
	$HBoxContainer/VBoxContainer2/AmbiantName.text = "%s: %s" % [category, sound_name]
	curr_ambiant_name = sound_name
	curr_ambiant_info = DataLoader.get_entry_data("Sounds", "Ambiant", category, sound_name)
	
	$AmbiantPlayer.set_stream(AudioFileLoader.loadfile(curr_ambiant_info["file"]))
	$AmbiantPlayer.play()
	
	$HBoxContainer/VBoxContainer2/AmbiantControls/PlayPause.set_state(1)


func play_sfx(category: String, sound_name: String):
	
	curr_sfx_name = sound_name
	curr_sfx_info = DataLoader.get_entry_data("Sounds", "SFX", category, sound_name)
	
	$SFXPlayer.set_stream(AudioFileLoader.loadfile(curr_sfx_info["file"]))
	set_stream_loop($SFXPlayer.stream, false)
	$SFXPlayer.play()
