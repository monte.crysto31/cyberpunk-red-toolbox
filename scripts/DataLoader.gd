extends Node


var DATA_FILE : String = "user://store.data"
var data : Dictionary = {
	"Sounds" : {
		"Music"   : {},
		"Ambiant" : {},
		"SFX"     : {}
	},
	"Sheets" : {
		"Players" : {},
		"NPCs"    : {},
	},
	"SubNETs" : {},
	"Maps"    : {}
}


func reset_section(section:String):
	
	match section:
		
		"Sounds":
			data["Sounds"] = {
				"Music"   : {},
				"Ambiant" : {},
				"SFX"     : {}
			}
		
		"Sheets":
			data["Sheets"] = {
				"Players" : {},
				"NPCs"    : {},
			}
		
		"SubNETs", "Maps":
			data[section] = {}


func add_category(data_type:String, data_subtype:Variant, data_cat:String):
	
	if data_subtype == null:
	
		if data_cat in data[data_type]:
			
			print("category %s/%s already exists!" % [data_type, data_cat])
			return
		
		data[data_type][data_cat] = {}
	
	else:
		
		if data_cat in data[data_type][data_subtype]:
			
			print("category %s/%s/%s already exists!" % [data_type, data_subtype, data_cat])
			return
		
		data[data_type][data_subtype][data_cat] = {}
	
	save_section(data_type)


func delete_category(data_type:String, data_subtype:Variant, data_cat:String):
	
	if data_subtype == null:
		data[data_type].erase(data_cat)
	
	else:
		data[data_type][data_subtype].erase(data_cat)
	
	save_section(data_type)


func add_entry(
	data_type:   String,
	data_subtype:Variant,
	data_cat:    String,
	entry_name:  String,
	entry_data:  Dictionary,
	overwrite = false
):
	
	if data_subtype == null:
	
		if entry_name in data[data_type][data_cat] and not overwrite:
			
			print("entry %s already exists in category %s/%s" % [entry_name, data_type, data_cat])
			return
		
		data[data_type][data_cat][entry_name] = entry_data
	
	else:
		
		if entry_name in data[data_type][data_subtype][data_cat] and not overwrite:
			
			print("entry %s already exists in category %s/%s/%s" % [entry_name, data_type, data_subtype, data_cat])
			return
		
		data[data_type][data_subtype][data_cat][entry_name] = entry_data
	
	save_section(data_type)


func delete_entry(
	data_type:   String,
	data_subtype:Variant,
	data_cat:    String,
	entry_name:  String
):
	
	if data_subtype == null:
		
		if data_cat in data[data_type]:
			data[data_type][data_cat].erase(entry_name)
	
	else:
		
		if data_cat in data[data_type][data_subtype]:
			data[data_type][data_subtype][data_cat].erase(entry_name)
	
	save_section(data_type)


func get_entry_data(
	data_type:   String,
	data_subtype:Variant,
	data_cat:    String,
	entry_name:  String
):
	
	return data[data_type][data_subtype][data_cat][entry_name]


func load_section(section:String):
	
	var FILE = ConfigFile.new()
	var err = FILE.load(DATA_FILE)
	
	reset_section(section)
	
	if err != OK:
		
		print("no data found in section %s, loading skipped" % section)
		return
	
	for key in FILE.get_section_keys(section):
		data[section][key] = FILE.get_value(section, key)
	
	#print("section %s loaded!" % section)

func load_data():
	
	var FILE = ConfigFile.new()
	var err = FILE.load(DATA_FILE)
	
	# reset data
	data = {
		"Sounds" : {
			"Music"   : {},
			"Ambiant" : {},
			"SFX"     : {}
		},
		"Sheets" : {
			"Players" : {},
			"NPCs"    : {},
		},
		"SubNETs" : {},
		"Maps"    : {}
	}
	
	# if there is no data file already, don't do anything
	if err != OK:
		
		print("no data found, loading skipped")
		return
	
	for data_type in FILE.get_sections():
		for key in FILE.get_section_keys(data_type):
		
			data[data_type][key] = FILE.get_value(data_type, key)
	
	print("data loaded!")


func save_section(section:String):
	
	# try to get an existing file, if not use a new one
	var FILE = ConfigFile.new()
	var err = FILE.load(DATA_FILE)
	if err != OK:
		FILE = ConfigFile.new()
	
	# save only the chosen section
	for key in data[section].keys():
		FILE.set_value(section, key, data[section][key])
	
	FILE.save(DATA_FILE)
	print("%s data saved!" % section)

func save_data():
	
	var FILE = ConfigFile.new()
	
	for data_type in data.keys():
		for key in data[data_type].keys():
			
			FILE.set_value(data_type, key, data[data_type][key])
	
	FILE.save(DATA_FILE)
	print("data saved!")
