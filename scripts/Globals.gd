extends Node


# player view window
enum {_EMPTY, NET, MAPS}
var player_view_state = _EMPTY
var is_player_view_visible : bool = false


# if anything requires parallel computing, use this number of threads
var nb_threads : int = floori(min(32, float(OS.get_processor_count()) * 0.5))
