extends Node

# goddamn netrunners I swear


func roll(rollstring: String) -> Array[int]:
	
	var dice = rollstring.split("d")
	var res: Array[int] = []
	
	for d in int(dice[0]):
		res.push_back(randi_range(1, int(dice[1])))
	
	return res

func roll_sum(rolls: Array[int]) -> int:
	
	var res: int = 0
	
	for r in rolls:
		res += r
	
	return res


var NET_lobby = [
	# difficulty: 0, 1, 2, 3
	[ ["File DD 4"],            ["File DD 6"],               ["File DD 8"],             ["File DD 10"]     ],
	[ ["Password DD 4"],        ["Password DD 6"],           ["Password DD 8"],         ["Password DD 10"] ],
	[ ["Password DD 6"],        ["Password DD 8"],           ["Password DD 10"],        ["Password DD 12"] ],
	[ ["Skunk"],                ["Skunk"],                   ["Skunk"],                 ["Skunk"]          ],
	[ ["Wisp"],                 ["Wisp"],                    ["Wisp"],                  ["Wisp"]           ],
	[ ["Killer"],               ["Killer"],                  ["Killer"],                ["Killer"]         ]
]

var NET_floors = [
	# difficulty: 0, 1, 2, 3
	# roll: 3 to 18
	[ ["Hellhound"],            ["Hellhound", "Hellhound"],  ["Kraken"],                ["Hellhound", "Hellhound", "Hellhound"]     ],
	[ ["Sabertooth"],           ["Hellhound", "Killer"],     ["Hellhound", "Scorpion"], ["Asp", "Asp"]                              ],
	[ ["Raven", "Raven"],       ["Skunk", "Skunk"],          ["Hellhound", "Killer"],   ["Hellhound", "Liche"]                      ],
	[ ["Hellhound"],            ["Sabertooth"],              ["Raven", "Raven"],        ["Wisp", "Wisp", "Wisp"]                    ],
	[ ["Wisp"],                 ["Scorpion"],                ["Sabertooth"],            ["Hellhound", "Sabertooth"]                 ],
	[ ["Raven"],                ["Hellhound"],               ["Hellhound"],             ["Kraken"]                                  ],
	[ ["Password DD 6"],        ["Password DD 8"],           ["Password DD 10"],        ["Password DD 12"]                          ],
	[ ["File DD 6"],            ["File DD 8"],               ["File DD 10"],            ["File DD 12"]                              ],
	[ ["Control Node DD 6"],    ["Control Node DD 8"],       ["Control Node DD 10"],    ["Control Node DD 12"]                      ],
	[ ["Password DD 6"],        ["Password DD 8"],           ["Password DD 10"],        ["Password DD 12"]                          ],
	[ ["Skunk"],                ["Asp"],                     ["Killer"],                ["Giant"]                                   ],
	[ ["Asp"],                  ["Killer"],                  ["Liche"],                 ["Dragon"]                                  ],
	[ ["Scorpion"],             ["Liche"],                   ["Dragon"],                ["Killer", "Scorpion"]                      ],
	[ ["Killer", "Skunk"],      ["Asp"],                     ["Asp", "Raven"],          ["Kraken"]                                  ],
	[ ["Wisp", "Wisp", "Wisp"], ["Raven", "Raven", "Raven"], ["Dragon", "Wisp"],        ["Raven", "Wisp", "Hellhound", "Hellhound"] ],
	[ ["Liche"],                ["Liche", "Raven"],          ["Giant"],                 ["Dragon", "Dragon"]                        ],
]

var NET_nodes = {
	"Password"     : {
		"desc" : "   A password is blocking your way! You have to crack it with a Pathfinder test, and can't see past it as long as you haven't beaten it with a score superior or equal to the value indicated by its DD.",
		"DD"   : 2,
		"img"  : preload("res://assets/net/Password.png"),
	},
	"File"         : {
		"desc" : "   An encrypted File, potentially bearing precious intel you need, or secret better not revealed. Well too bad it's encrypted then!... Or is it? Decrypt and reveal its contents with an Eye-Dee test greater or equal to the the DD indicated by the File.",
		"DD"   : 2,
		"img"  : preload("res://assets/net/File.png"),
	},
	"Control Node" : {
		"desc" : "   Camera? Turret? Whatever it is, you can try taking control of it on this floor by attempting a Control test and scoring at least higher than the DD of this Control Node. You then instantly gain access to whatever is linked to this Node so you can help your friends... Or trap your foes.",
		"DD"   : 2,
		"img"  : preload("res://assets/net/Control Node.png"),
	},
}

var Programs = {
	"Eraser" : {
		"class" : "Booster",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 7, 
		"desc"  : "   Increases all Cloack checks you make by +2 as long as this Program remains Rezzed",
		"cost"  : 20,
		"img"   : preload("res://assets/net/programs/Eraser.png")
	},
	"See Ya" : {
		"class" : "Booster",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 7, 
		"desc"  : "   Increases all Pathfinder checks you make by +2 as long as this Program remains Rezzed",
		"cost"  : 20,
		"img"   : preload("res://assets/net/programs/See Ya.png")
	},
	"Speedy Gonzalvez" : {
		"class" : "Booster",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 7, 
		"desc"  : "   Increases your Speed by +2 as long as this Program is Rezzed",
		"cost"  : 100,
		"img"   : preload("res://assets/net/programs/Speedy Gonzalvez.png")
	},
	"Worm" : {
		"class" : "Booster",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 7, 
		"desc"  : "   Increases all Backdoor checks you make by +2 as long as this program remains Rezzed",
		"cost"  : 50,
		"img"   : preload("res://assets/net/programs/Worm.png")
	},
	"Armor" : {
		"class" : "Defender",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 7, 
		"desc"  : "   Lowers all brain damage by 4 as long as this program remains Rezzed.\n   Only 1 copy can be running at a time.\n   Each copy can only be used once per Netrun",
		"cost"  : 50,
		"img"   : preload("res://assets/net/programs/Armor.png")
	},
	"Flak" : {
		"class" : "Defender",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 7, 
		"desc"  : "   Reduces the ATK of all Non-Black ICE Attacker Programs run against you to 0 as long as Rezzed.\n   Only 1 copy can be running at a time.\n   Each copy can only be used once per Netrun",
		"cost"  : 50,
		"img"   : preload("res://assets/net/programs/Flak.png")
	},
	"Shield" : {
		"class" : "Defender",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 7, 
		"desc"  : "   Stops the first successful Non-Black ICE Program Effect from dealing brain damage.\n   After that, the Shield Derezzes itself.\n   Only 1 copy can be running at a time.\n   Each copy can only be used once per Netrun",
		"cost"  : 20,
		"img"   : preload("res://assets/net/programs/Shield.png")
	},
	"Banhammer" : {
		"class" : "Anti-Program Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 1,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Deals 3d6 REZ to a Non-Black ICE Program, or 2d6 to a Black ICE Program",
		"cost"  : 50,
		"img"   : preload("res://assets/net/programs/Banhammer.png")
	},
	"Sword" : {
		"class" : "Anti-Program Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 1,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Deals 3d6 REZ to a Black ICE Program, or 2d6 to a Non-Black ICE Program",
		"cost"  : 50,
		"img"   : preload("res://assets/net/programs/Sword.png")
	},
	"DeckKRASH" : {
		"class" : "Anti-Personnel Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Enemy Netrunner is forcibly and unsafely Jacked Out of the Architecture, suffering the effect of all Rezzed enemy Black ICE they've encountered in the Architecture as they leave",
		"cost"  : 100,
		"img"   : preload("res://assets/net/programs/DeckKRASH.png")
	},
	"Hellbolt" : {
		"class" : "Anti-Personnel Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 2,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Deals 2d6 Damage direct to the enemy Netrunner's brain.\n   Unless insulated, their Cyberdeck catches fire along with their clothing.\n   Until they spend a Meat Action to put themselves out, they take 2 Damage to their HP whenever they end their Turn.\n   Multiple instances of this effet cannot stack",
		"cost"  : 100,
		"img"   : preload("res://assets/net/programs/Hellbolt.png")
	},
	"Nervescrub" : {
		"class" : "Anti-Personnel Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Enemy Netrunner's INT, REF and DEX are each lowered by 1d6 for the next hour (minimum 1).\n   The effects are largely psychosomatic and leave no permanent effects",
		"cost"  : 100,
		"img"   : preload("res://assets/net/programs/Nervescrub.png")
	},
	"Poison Flatline" : {
		"class" : "Anti-Personnel Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 0,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Destroys a single Non-Black ICE Program installed on the Netrunner target's Cyberdeck at random",
		"cost"  : 100,
		"img"   : preload("res://assets/net/programs/Poison Flatline.png")
	},
	"Superglue" : {
		"class" : "Anti-Personnel Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 2,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Enemy Netrunner cannot progress deeper into the Architecture or Jack Out safely for 1d6 Rounds (enemy Netrunner can still perform an unsafe Jack Out, though).\n   Each copy of this Program can only be used once per Netrun",
		"cost"  : 100,
		"img"   : preload("res://assets/net/programs/Superglue.png")
	},
	"Vrizzbolt" : {
		"class" : "Anti-Personnel Attacker",
		"PER"   : -1,
		"SPD"   : -1,
		"ATK"   : 1,
		"DEF"   : 0,
		"REZ"   : 0, 
		"desc"  : "   Deals 1d6 Damage direct to a Netrunner's brain and lowers the amount of total NET Action the Netrunner can accomplish on their next Turn by 1 (minimum 2)",
		"cost"  : 50,
		"img"   : preload("res://assets/net/programs/Vrizzbolt.png")
	},
	"Asp" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 4,
		"SPD"   : 6,
		"ATK"   : 2,
		"DEF"   : 2,
		"REZ"   : 15, 
		"desc"  : "   Destroys a single Program installed on the enemy Netrunner's Cyberdeck at random",
		"cost"  : 100,
		"img"   : preload("res://assets/net/ice/Asp.png")
	},
	"Giant" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 2,
		"SPD"   : 2,
		"ATK"   : 8,
		"DEF"   : 4,
		"REZ"   : 25,
		"desc"  : "   Deals 3d6 Damage direct to an enemy Netrunner's brain.\n   The Netrunner is forcibly and unsafely Jacked Out of their current Netrun.\n   They suffer the effect of all Rezzed enemy Black ICE they've encountered in the Architecture as they leave, not including the Giant",
		"cost"  : 1000,
		"img"   : preload("res://assets/net/ice/Giant.png")
	},
	"Hellhound" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 6,
		"SPD"   : 6,
		"ATK"   : 6,
		"DEF"   : 2,
		"REZ"   : 20, 
		"desc"  : "   Deals 2d6 Damage direct to the Netrunner's brain.\n   Unless insulated, their Cyberdeck catches fire along with their clothing.\n   Until they spend a Meat Action to put themselves out, they take 2 Damage to their HP whenever they end their Turn.\n   Multiple instances of theis effect cannot stack",
		"cost"  : 500,
		"img"   : preload("res://assets/net/ice/Hellhound.png")
	},
	"Kraken" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 6,
		"SPD"   : 2,
		"ATK"   : 8,
		"DEF"   : 4,
		"REZ"   : 30, 
		"desc"  : "   Deals 3d6 Damage direct to an enemy Netrunner's brain.\n   Until the end of the Netrunner's next Turn, the Netrunner cannot progress deeper into the Architecture or Jack Out safely (the Netrunner can still perform an unsafe Jack Out)",
		"cost"  : 1000,
		"img"   : preload("res://assets/net/ice/Kraken.png")
	},
	"Liche" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 8,
		"SPD"   : 2,
		"ATK"   : 6,
		"DEF"   : 2,
		"REZ"   : 25, 
		"desc"  : "   Enemy Netrunner's INT, REF and DEX are each lowered by 1d6 for the next hour (minimum 1).\n   The effects are largely psychosomatic and leave no permanent effect",
		"cost"  : 500,
		"img"   : preload("res://assets/net/ice/Liche.png")
	},
	"Raven" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 6,
		"SPD"   : 4,
		"ATK"   : 4,
		"DEF"   : 2,
		"REZ"   : 15, 
		"desc"  : "   Derezzes a single Defender Program the Netrunner has Rezzed at random, then deals 1d6 Damage direct to the Netrunner's brain",
		"cost"  : 50,
		"img"   : preload("res://assets/net/ice/Raven.png")
	},
	"Scorpion" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 2,
		"SPD"   : 6,
		"ATK"   : 2,
		"DEF"   : 2,
		"REZ"   : 15, 
		"desc"  : "   Enemy Netrunner's MOVE is lowered by 1d6 for the next hour (minimum 1).\n   The effects are largely psychosomatic and leave no permanent effects",
		"cost"  : 100,
		"img"   : preload("res://assets/net/ice/Scorpion.png")
	},
	"Skunk" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 2,
		"SPD"   : 4,
		"ATK"   : 4,
		"DEF"   : 2,
		"REZ"   : 10, 
		"desc"  : "   Until this Program is Derezzed, an enemy Netrunner hit by this Effect makes all Slide Checks at a -2.\n   Each Skunk Black ICE can only affect a single Netrunner at a time, but the effects of multiple Skunks can stack",
		"cost"  : 500,
		"img"   : preload("res://assets/net/ice/Skunk.png")
	},
	"Wisp" : {
		"class" : "Anti-Personnel Black ICE",
		"PER"   : 4,
		"SPD"   : 4,
		"ATK"   : 4,
		"DEF"   : 2,
		"REZ"   : 15, 
		"desc"  : "   Deals 1d6 Damage direct to the enemy Netrunner's brain and lowers the amout of total NET Actions the Netrunner can accomplish on their next Turn by 1 (minimum 2)",
		"cost"  : 50,
		"img"   : preload("res://assets/net/ice/Wisp.png")
	},
	"Dragon" : {
		"class" : "Anti-Program Black ICE",
		"PER"   : 6,
		"SPD"   : 4,
		"ATK"   : 6,
		"DEF"   : 6,
		"REZ"   : 30, 
		"desc"  : "   Deals 6d6 REZ to a Program.\n   If this damage would be enough to Derezz the Program, it is instead Destroyed",
		"cost"  : 1000,
		"img"   : preload("res://assets/net/ice/Dragon.png")
	},
	"Killer" : {
		"class" : "Anti-Program Black ICE",
		"PER"   : 4,
		"SPD"   : 8,
		"ATK"   : 6,
		"DEF"   : 2,
		"REZ"   : 20, 
		"desc"  : "   Deals 4d6 REZ to a Program.\n   If this damage would be enough to Derezz the Program, it is instead Destroyed",
		"cost"  : 500,
		"img"   : preload("res://assets/net/ice/Killer.png")
	},
	"Sabertooth" : {
		"class" : "Anti-Program Black ICE",
		"PER"   : 8,
		"SPD"   : 6,
		"ATK"   : 6,
		"DEF"   : 2,
		"REZ"   : 25, 
		"desc"  : "   Deals 6d6 REZ to a Program.\n   If this damage would be enough to Derezz the Program, it is instead Destroyed",
		"cost"  : 1000,
		"img"   : preload("res://assets/net/ice/Sabertooth.png")
	}
}

var Demons = {
	"Imp"    : {
		"REZ" : 15,
		"Interface" : 3,
		"NET Actions" : 2,
		"Combat Value" : 14,
		"desc" : "",
		"cost" : 1000,
		"img" : preload("res://assets/net/demons/Imp.png"),
	},
	"Efreet" : {
		"REZ" : 25,
		"Interface" : 4,
		"NET Actions" : 3,
		"Combat Value" : 14,
		"desc" : "",
		"cost" : 5000,
		"img" : preload("res://assets/net/demons/Efreet.png"),
	},
	"Balron" : {
		"REZ" : 30,
		"Interface" : 7,
		"NET Actions" : 4,
		"Combat Value" : 14,
		"desc" : "",
		"cost" : 10000,
		"img" : preload("res://assets/net/demons/Balron.png"),
	},
}
