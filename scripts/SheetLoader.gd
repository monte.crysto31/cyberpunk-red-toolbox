extends Node


var SHEETS_FILE = "user://sheets.data"
var sheets : Dictionary = {
	"Players" : {},
	"NPCs"    : {},
}


func add_category(sheet_type: String, cat_name: String):
	
	if sheets[sheet_type].keys().has(cat_name):
		
		print("category %s already exists!" % cat_name)
		return
	
	sheets[sheet_type][cat_name] = {}
	#print("added category %s/%s" % [sheet_type, cat_name])
	
	save_sheets()


func delete_category(sheet_type: String, cat_name: String):
	
	sheets[sheet_type].erase(cat_name)
	save_sheets()


func add_sheet(
	sheet_type: String,
	cat_name:   String,
	sheet_name: String,
	sheet_data: Dictionary,
	overwrite = false
):
	
	if sheets[sheet_type][cat_name].keys().has(sheet_name) and not overwrite:
		
		print("sound %s already exists in category %s!" % [sheet_name, cat_name])
		return
	
	sheets[sheet_type][cat_name][sheet_name] = sheet_data
	
	save_sheets()


func delete_sheet(sheet_type: String, cat_name: String, sheet_name: String):
	
	if sheets[sheet_type].keys().has(cat_name):
		
		sheets[sheet_type][cat_name].erase(sheet_name)
		save_sheets()


func get_sheet_info(sheet_type: String, cat_name: String, sheet_name: String):
	
	return sheets[sheet_type][cat_name][sheet_name]


func load_sheets():
	
	var FILE = ConfigFile.new()
	var err = FILE.load(SHEETS_FILE)
	
	# reset sheets
	sheets = {
		"Players" : {},
		"NPCs"    : {},
	}
	
	# if there is no sheets file already, don't do anything
	if err != OK:
		
		print("no sheet found")
		return
	
	for sheet_type in FILE.get_sections():
		for cat_name in FILE.get_section_keys(sheet_type):
		
			sheets[sheet_type][cat_name] = FILE.get_value(sheet_type, cat_name)
	
	print("sheets loaded!")
	#print(sheets)


func save_sheets():
	
	var FILE = ConfigFile.new()
	
	for sheet_type in sheets.keys():
		for cat_name in sheets[sheet_type].keys():
			
			FILE.set_value(sheet_type, cat_name, sheets[sheet_type][cat_name])
	
	FILE.save(SHEETS_FILE)
	print("sheets saved!")
