extends Node


# display control

signal change_widget(name:String)
signal show_remote_NET(subnet_display:Control)

signal show_remote_map      (map:Map)
signal move_remote_map      (pos:Vector2, zoom:float)
signal move_remote_token    (old_coords:Vector2i, new_coords:Vector2i, texture:ImageTexture)
signal refresh_remote_fow   (new_fow:Image)
signal refresh_remote_tokens(new_tokens:Dictionary)

# audio control

signal play_music  (category:String, sound_name:String)
signal play_ambiant(category:String, sound_name:String)
signal play_sfx    (category:String, sound_name:String)


func _process(_delta):
	
	if Input.is_action_just_pressed("ui_toggle_fullscreen"):
		
		if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
			
		else:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN)
