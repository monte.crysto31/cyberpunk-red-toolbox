class_name ActivatedIndicator
extends TextureRect


@export var texture_on : Texture2D
var texture_off : Texture2D


func _ready():
	
	self.texture_off = self.texture


func _process(_delta):
	
	if condition():
		self.texture = self.texture_on
	else:
		self.texture = self.texture_off


# MUST BE IMPLEMENTED
func condition() -> bool:
	return false
