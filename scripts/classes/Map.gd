class_name Map


var category   : String = ""
var name       : String = ""
var layers     : Array[MapLayer] = []
var curr_layer : int = -1


func _init():
	
	self.add_layer()


func get_size() -> Vector2i:
	return self.layers[self.curr_layer].map_size

func get_cell_size():
	return self.layers[self.curr_layer].cell_size

func get_background():
	if self.layers[self.curr_layer].background == "":
		return null
	else:
		return ImageTexture.create_from_image(
			Image.load_from_file(
				self.layers[self.curr_layer].background))


func switch_layer(index:int):
	
	if index in range(self.layers.size()):
		self.curr_layer = index
	else:
		self.curr_index = -1

func add_layer(layer_name:String = "Layer 0"):
	
	var new_layer = MapLayer.new()
	new_layer.name  = layer_name
	new_layer.index = self.layers.size()
	
	self.layers.push_back(new_layer)
	switch_layer(new_layer.index)

func delete_layer(index:int):
	
	self.layers.pop_at(index)
	
	# re-index layers to not leave a hole in the numbering
	for i in range(index, self.layers.size()):
		self.layers[i].index = i
	
	if self.curr_layer >= index:
		self.curr_layer -= 1
	
	# if we deleted the last one, create another
	if self.layers.size() == 0:
		add_layer()

func rename_layer(index:int, new_name:String):
	
	self.layers[index].name = new_name

func resize_layer(new_size:Vector2i):
	
	self.layers[self.curr_layer].resize(new_size)

func set_layer_background(path:String):
	
	self.layers[self.curr_layer].background = path

func resize_cells(new_size:float):
	
	self.layers[self.curr_layer].cell_size = new_size


func get_zoom():
	return self.layers[self.curr_layer].zoom

func set_zoom(new_zoom:float):
	self.layers[self.curr_layer].zoom = new_zoom


func set_cell(coords:Vector2i, data:Variant):
	
	if data == null:
		self.layers[self.curr_layer].new_tokens.erase(coords)
		return
	
	self.layers[self.curr_layer].new_tokens[coords] = data

func get_cell(coords:Vector2i):
	
	return self.layers[self.curr_layer].new_tokens.get(coords, null)


func get_tokens():
	
	return self.layers[self.curr_layer].new_tokens


func set_fow(image:Image):
	
	self.layers[self.curr_layer].fow = image.duplicate()

func get_fow() -> Image:
	
	return self.layers[self.curr_layer].fow


func duplicate():
	
	var new_map : Map = Map.new()
	
	new_map.category   = self.category
	new_map.name       = self.name
	new_map.layers     = []
	new_map.curr_layer = self.curr_layer
	
	for layer in self.layers:
		new_map.layers.push_back(layer.duplicate())
	
	return new_map

func serialize():
	
	var layers_data = []
	for layer in self.layers:
		layers_data.push_back(layer.serialize())
	
	return {
		"category"   : self.category,
		"name"       : self.name,
		"layers"     : layers_data,
		"curr_layer" : self.curr_layer
	}

func deserialize(data:Dictionary):
	
	self.category   = data.get("category", "toto")
	self.name       = data.get("name", "Some Map")
	self.curr_layer = data.get("curr_layer", -1)
	self.layers     = []
	
	for layer_data in data.get("layers", []):
		
		var layer = MapLayer.new()
		layer.deserialize(layer_data)
		self.layers.push_back(layer)
	
	if self.layers.size() == 0:
		add_layer()
