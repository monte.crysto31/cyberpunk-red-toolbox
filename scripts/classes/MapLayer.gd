class_name MapLayer


var name       : String     = ""
var index      : int        = 0
var background : String     = ""
var fow_res    : Vector2    = Vector2(500., 500.)
var fow_ppc    : float      = 25.
var fow        : Image      = null
var map_size   : Vector2i   = Vector2i(20, 20)
var cell_size  : float      = 100.
var zoom       : float      = 1.
var new_tokens : Dictionary = {} # dictionnary of existing (x,y) -> token


func _init():
	
	# create a fow image at a certain pixels per cell (ppc)
	self.fow_res = self.map_size * self.fow_ppc
	
	create_fow()
	
	resize(self.map_size)


func create_fow():
	
	self.fow = Image.create(floori(self.fow_res.x), floori(self.fow_res.y), false, Image.FORMAT_L8)
	self.fow.fill(Color.BLACK)


func resize(new_size:Vector2i):
	
	self.map_size = new_size
	
	# tokens
	
	# delete tokens outside of the new size
	for coords in self.new_tokens.keys():
		if coords.x >= self.map_size.y or coords.y >= self.map_size.x:
			self.new_tokens.erase(coords)
	
	# fog of war
	
	self.fow_res = Vector2(self.map_size) * self.fow_ppc
	
	self.fow.resize(
		floori(self.fow_res.x),
		floori(self.fow_res.y))


func duplicate():
	
	var new_layer = MapLayer.new()
	
	new_layer.name       = self.name
	new_layer.index      = self.index
	new_layer.background = self.background
	new_layer.fow_res    = self.fow_res
	new_layer.fow_ppc    = self.fow_ppc
	new_layer.map_size   = self.map_size
	new_layer.cell_size  = self.cell_size
	new_layer.zoom       = self.zoom
	
	new_layer.new_tokens = self.new_tokens.duplicate()
	new_layer.fow        = Image.create_from_data(
		self.fow.get_width(),
		self.fow.get_height(),
		self.fow.has_mipmaps(),
		self.fow.get_format(),
		self.fow.get_data())
	
	return new_layer

func serialize():
	
	return {
		"name"       : self.name,
		"index"      : self.index,
		"background" : self.background,
		"fow_res"    : self.fow_res,
		"fow_ppc"    : self.fow_ppc,
		"fow"        : self.fow.duplicate(),
		"map_size"   : self.map_size,
		"cell_size"  : self.cell_size,
		"zoom"       : self.zoom,
		"new_tokens" : self.new_tokens,
	}

func deserialize(data:Dictionary):
	
	self.name       = data.get("name",       "Layer")
	self.index      = data.get("index",      0)
	self.background = data.get("background", "")
	self.fow_res    = data.get("fow_res",    Vector2(500., 500.))
	self.fow_ppc    = data.get("fow_ppc",    25.)
	self.fow        = data.get("fow",        null)
	self.map_size   = data.get("map_size",   Vector2i(20, 20))
	self.cell_size  = data.get("cell_size",  100.)
	self.zoom       = data.get("zoom",       1.0)
	self.new_tokens = data.get("new_tokens", {})
	
	if self.fow == null:
		print("created FOW image")
		create_fow()
	
	resize(self.map_size)
