class_name NavButton
extends Button


func _ready():
	
	connect("pressed", SignalBus.emit_signal.bind("change_widget", self.text))
