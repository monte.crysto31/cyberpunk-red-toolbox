class_name SubNET

var name         : String       = ""
var category     : String       = ""
var difficulty   : int          = 0
var nb_floors    : int          = 0
var nb_branches  : int          = 0
var curr_floor   : Vector2i     = Vector2i(0, 0)
var demon        : String       = ""
var floors       : Dictionary   = {}
var branches     : Array[int]   = []
var branch_start : Array[int]   = []
var branch_stop  : Array[int]   = []
var turns        : Array[int]   = []
var net_matrix   : Array[Array] = []


# use curr_floor and the net_matrix to move arounf in the NET

func curr_floor_to_str() -> String:
	
	return "%s_%s" % [curr_floor.x+1, net_matrix[curr_floor.x][curr_floor.y]]

func move_up() -> String:
	
	# move up by one if we can, if not we don't do anything
	if curr_floor.x > 0:
		
		curr_floor.x -= 1
		
		# if we hit the top of the branch, we need to seek the parent branch to
		# the left, which we will ALWAYS find since branches split to the right 
		if net_matrix[curr_floor.x][curr_floor.y] == -1:
			while net_matrix[curr_floor.x][curr_floor.y] < 0:
				curr_floor.y -= 1
		
		# otherwise we don't have anything else to do
	
	return curr_floor_to_str()

func move_down() -> String:
	
	if curr_floor.x < (nb_floors-1) and net_matrix[curr_floor.x+1][curr_floor.y] > 0:
		curr_floor.x += 1
	
	return curr_floor_to_str()

func move_right():
	
	# we have a link to the right, but don't know which one yet
	if curr_floor.y < (net_matrix[0].size()-1) and net_matrix[curr_floor.x][curr_floor.y+1] < 0:
		
		curr_floor.y += 1
		
		# seek an angle to the right
		while net_matrix[curr_floor.x][curr_floor.y] == -2:
			curr_floor.y += 1
		
		# here we have found an angle, or had one to begin with so we can go down
		curr_floor.x += 1
	
	return curr_floor_to_str()


func roll_all_floors():

	for f in self.floors.keys():

		# lobby floor?
		if ["1_1", "2_1", "3_1"].has(f):
			self.floors[f] = RollTables.NET_lobby[RollTables.roll("1d6")[0] - 1][self.difficulty]
		
		# other floor?
		else:
			self.floors[f] = RollTables.NET_floors[RollTables.roll_sum(RollTables.roll("3d6")) - 3][self.difficulty]

func generate():
	
	print("\n\nSetting up subnet:\n")

	self.nb_floors    = RollTables.roll_sum(RollTables.roll("3d6"))
	self.nb_branches  = 1
	self.curr_floor   = Vector2i(0, 0)
	self.demon        = ""
	self.floors       = {}
	self.branch_start = []
	self.branch_stop  = []
	self.turns        = []
	self.net_matrix   = []

	# determine number of branches. only floors 3 and onwards can split,
	# and the last floor can't split.
	# A branch can only split in two, and there will only be one split by floor
	# for simplicity so there may be no more than (nb_floors - 3) branches
	
	while (RollTables.roll("1d10")[0]  >= 7) and (self.nb_branches < self.nb_floors - 3):
		self.nb_branches += 1
	
	# Determines which floors will split.
	
	self.branch_start.resize(self.nb_floors)
	self.branch_start.fill(0)
	
	var remaining = self.nb_branches - 1
	
	while remaining > 0:
		
		var s = randi_range(3, self.nb_floors-1)
		
		if self.branch_start[s - 1] == 0:
			
			self.branch_start[s - 1] = self.nb_branches - remaining + 1
			remaining = remaining - 1
	
	# ending floor of each branch
	self.branch_stop.resize(self.nb_floors)
	self.branch_stop.fill(-1)
	
	for f in self.nb_floors:
		if self.branch_start[f] != 0:
			self.branch_stop[f] = randi_range(f + 2, self.nb_floors)
	
	print("branches start/stop:\n", self.branch_start, "\n", self.branch_stop)
	
	# init floors/branches
	
	for f in self.nb_floors:
		self.floors["%s_1" % str(f+1)] = []
	
	var curr_branch = 2
	
	for f in range(2, self.nb_floors):
		
		if self.branch_start[f] != 0:
			
			#print("from floor", f + 2, "to", self.branch_stop[f])
			for tmp_f in range(f + 2, self.branch_stop[f] + 1):
				
				self.floors[ "%s_%s" % [str(tmp_f), str(curr_branch)] ] = []
			
			curr_branch += 1
	
	# debug
	print("floors:", self.floors.keys())
	
	# how the main branch turns : 0=straight, 1=turn
	# cumulated offset of main branch 1 on floor f is sum of turns from 0 to f-1
	
	for i in self.branch_start.size():
		if self.branch_start[i] != 0:
			self.turns.push_back(randi_range(0, 1))
	
	# debug
	print("turns:" , self.turns)
	
	# compute net matrix, for rendering
	
	# if only one branch, no brainer
	if(self.nb_branches == 1):
		
		self.net_matrix.resize(self.nb_floors)
		self.net_matrix.fill([1])
	
	# if more than 1 branch, order/offset them
	else:
		
		var tmp = Array()
		tmp.resize(self.nb_branches)
		tmp.fill(0)
		tmp.push_front(1)
		
		for f in self.nb_floors:
			self.net_matrix.push_back(tmp.duplicate())
		
		var main_at     = 0
		var last_branch = 1

		# FIRST PASS: rearrange branches according to main branch split directions
		for f in self.nb_floors:

			# oh, there is a branch starting on that floor ^^
			if self.branch_start[f] != 0:

				#print("new branch", last_branch, "at floor", f, "at col", main_at)

				# if there is a branch overlapping we offset it before resuming
				if (self.net_matrix[f][main_at + 1] != 0):

					#print("offsetting branch at position", main_at + 1, "by one")

					for ff in self.nb_floors:
						self.net_matrix[ff].insert(main_at + 1, 0)

				# the rest continues as normal

				# exchange branches order from floor f and onwards if main branch turns
				if(self.turns[last_branch - 1] == 1):

					#print("exchanging branches at positions", main_at, "and", main_at + 1)
					
					# offset main branch
					for ff in range(f + 1, self.nb_floors):
						self.net_matrix[ff][main_at + 1] = 1
						self.net_matrix[ff][main_at] = 0
					
					# print secondary branch straight down
					for ff in range(f + 1, self.branch_stop[f]):
						self.net_matrix[ff][main_at] = last_branch + 1
					
					main_at += 1
				
				# else, the main branch continues straight and we just add the new branch on the side
				else:
					
					#print("new branch", last_branch + 1, "is at column", main_at + 1)
					
					for ff in range(f + 1, self.branch_stop[f]):
						self.net_matrix[ff][main_at + 1] = last_branch + 1                           
				
				last_branch += 1
		
		# SECOND PASS: move branches to the left as much as we can
		# they need to be protected (with 0s) on their left for all their length
		# plus one above and below, and they need to have diverged to the right
		# of the main branch (source: Trust Me Bro (TM))
		
		# branch id: we start at branch 2
		var branch = 2
		
		while branch <= self.nb_branches:
			
			# if the main branch turned we don't care
			if self.turns[branch-2] == 1:
				branch += 1
				continue
			
			
			# get branch starting position
			var branch_pos : Vector2i = Vector2i.ZERO
			for f in self.nb_floors:
				if self.net_matrix[f].has(branch):
					branch_pos = Vector2i(f, self.net_matrix[f].find(branch))
					break
			
			var branch_length: int = self.branch_stop[branch_pos.x-1] - branch_pos.x
			
			print("pos/length: ", branch_pos, "/", branch_length)
			
			# check on the column to the left if we have room to move
			var left_is_clear: bool = true
			for f in range(branch_pos.x-1, min(branch_pos.x + branch_length + 1, self.nb_floors)):
				print(f,",",branch_pos.y-1, " : ", self.net_matrix[f][branch_pos.y-1])
				left_is_clear = left_is_clear and self.net_matrix[f][branch_pos.y-1] == 0
			
			# offset branch to the left by one if it's clear
			if left_is_clear:
				for f in branch_length:
					self.net_matrix[f + branch_pos.x][branch_pos.y-1] = branch
					self.net_matrix[f + branch_pos.x][branch_pos.y  ] = 0
			
			else:
				branch += 1
		
		# THIRD PASS: remove useless columns of zeroes on the right
		
		var compact = false
		
		while not compact:
			
			for f in self.nb_floors:
				compact = (self.net_matrix[f][self.net_matrix[f].size() - 1] != 0) or compact
			
			if compact:
				break
			
			for f in self.nb_floors:
				self.net_matrix[f].pop_back()
		
		# FOURTH PASS: replace zeroes with negative values for the different
		# graph links (AngleLine -> -1, HLine -> -2, Vline -> -3 (not used yet))
		
		# Ignore the last floor becaus there can be no links there, only floors,
		# and the first column because the tree only splits towards the right
		for f in (self.net_matrix.size()-1):
			for c in range(1, self.net_matrix[0].size()):
				
				# Angles appear when there is the beginning of a branch below
				# but nothing on the current cell
				if self.net_matrix[f][c] == 0 and self.net_matrix[f+1][c] > 0:
					
					self.net_matrix[f][c] = -1
					
					# fill all zeroes to the left with HLines until we hit the
					# parent branch
					for i in c:
						if self.net_matrix[f][c-i-1] == 0:
							self.net_matrix[f][c-i-1] = -2
						else:
							break
					
					# there can only be one split per floor, so we jump to the
					# next one
					continue
	
	
	
		
	# debug
	print("net matrix:")
	for f in range(self.nb_floors):
		print(self.net_matrix[f])

	# populate floors
	self.roll_all_floors()

	# debug print            
	#print("branches start:"  , self.branch_start)
	#print("branches stop at:", self.branch_stop)


func serialize_NET() -> Dictionary:
	
	return {
		"name"         : self.name,
		"category"     : self.category,
		"difficulty"   : self.difficulty,
		"nb_floors"    : self.nb_floors,
		"nb_branches"  : self.nb_branches,
		"curr_floor"   : self.curr_floor,
		"demon"        : self.demon,
		"floors"       : self.floors,
		"branches"     : self.branches,
		"branch_start" : self.branch_start,
		"branch_stop"  : self.branch_stop,
		"turns"        : self.turns,
		"net_matrix"   : self.net_matrix,
	}

func deserialize_NET(
	net_category:String,
	net_name:String,
	data:Dictionary) -> void:
	
	self.name         = net_name
	self.category     = net_category
	self.difficulty   = data.get("difficulty",    0)
	self.nb_floors    = data.get("nb_floors",     0)
	self.nb_branches  = data.get("nb_branches",   0)
	self.curr_floor   = data.get("curr_floor",   Vector2i(0, 0))
	self.demon        = data.get("demon",        "")
	self.floors       = data.get("floors",       {})
	self.branches     = data.get("branches",     Array([], TYPE_INT,   &"", null))
	self.branch_start = data.get("branch_start", Array([], TYPE_INT,   &"", null))
	self.branch_stop  = data.get("branch_stop",  Array([], TYPE_INT,   &"", null))
	self.turns        = data.get("turns",        Array([], TYPE_INT,   &"", null))
	self.net_matrix   = data.get("net_matrix",   Array([], TYPE_ARRAY, &"", null))

