class_name Widget
extends Control


@export var WidgetName : String


func toggle(new_widget: String) -> void:
	
	if new_widget == WidgetName:
		self.update()
		self.show()
		
	else:
		self.hide()


# update categories and whatnot
func update():
	
	pass
